<?php

function get_typed_question($score_csv, $grp_csv, $resp_csv = null){
    $returned_values = array('question' => array(), 'id_col' => array(), 'uvs' => array(), 'grp' => array());
    $regexp = '/[,\' \wàâáçéèèêëìîíïôòóùûüÂÊÎÔúÛÄËÏÖÜÀÆæÇÉÈŒœÙñý]+_[\w]+[\w ]+/i';
    $score_data = fgetcsv($score_csv, 10000, ',');
    foreach ($score_data as $id_col => $data) {
        if(preg_match($regexp, $data)){
            array_push($returned_values['question'], $data);
            array_push($returned_values['id_col'], $id_col);
        }
    }
    if($resp_csv !== null){
        while (($resp_data = fgetcsv($resp_csv, 10000, ',')) !== FALSE) {
            if(strtolower($resp_data[0]) === 'id'){
                $returned_values['uvs'] = $resp_data;
                break;
            }
        }
    }
    $i_line_grp = 0;
    $index_id = -1;
    while (($grp_data = fgetcsv($grp_csv, 10000, ',')) !== FALSE) {
        if($i_line_grp === 0){
            $index_id = array_search('id', $grp_data);
        }else{
            array_push($returned_values['grp'], $grp_data[$index_id]);
        }
        $i_line_grp++;
    }
    return $returned_values;
}

function check_uv($link_csv){
    $not_found_uv = array('uvs' => array(), 'id_col' => array(), 'questions' => array());
    $have_not_found = false;
    $questions = $link_csv['question'];
    $id_col = $link_csv['id_col'];
    $uvs = array_map('strtolower', $link_csv['uvs']);
    foreach($questions as $ref_col => $question){
        $data_explode = explode('_', $question);
        $uvs_in_quest = explode(' ', $data_explode[count($data_explode) - 1]);
        array_shift($uvs_in_quest);
        foreach($uvs_in_quest as $uv){
            $current_uv = strtolower($uv);
            if(!in_array($current_uv, $uvs) && $current_uv !== ''){
                array_push($not_found_uv['uvs'], $uv);
                array_push($not_found_uv['questions'], $question);
                array_push($not_found_uv['id_col'], $id_col[$ref_col]);
                $have_not_found = true;
            }
        }
    }
    if($have_not_found){
        return $not_found_uv;
    }else{
        return $have_not_found;
    }
}

function check_group_question($link_csv){
    $not_found_grp = array('grp' => array(), 'id_col' => array(), 'questions' => array());
    $have_not_found = false;
    $questions = $link_csv['question'];
    $id_col = $link_csv['id_col'];
    $grps = array_map('strtolower', $link_csv['grp']);
    foreach($questions as $ref_col => $question){
        $data_explode = explode('_', $question);
        $uvs_and_quest_in_quest = explode(' ', $data_explode[count($data_explode) - 1]);
        $grp_in_quest = $uvs_and_quest_in_quest[0];
        if(!in_array('strtolower'($grp_in_quest), $grps)){
            array_push($not_found_grp['grp'], $grp_in_quest);
            array_push($not_found_grp['questions'], $question);
            array_push($not_found_grp['id_col'],  $id_col[$ref_col]);
            $have_not_found = true;
        }
    }    
    if($have_not_found){
        return $not_found_grp;
    }else{
        return $have_not_found;
    }
}

function check_col_exist($csv, array $cols){
    $col_missing = array();
    $header_csv = fgetcsv($csv, 10000, ',');
    $header_csv = array_map('trim', $header_csv);
    foreach ($cols as $col) {
        if(!in_array($col, $header_csv)){
            array_push($col_missing, $col);
        }
    }
    if(count($col_missing) > 0){
        return $col_missing;
    }else{
        return false;
    }
}

function check_score_col($core_csv){
    $cols = array('Prénom', 'Nom', 'Adresse de courriel', 'Total du cours');
    return check_col_exist($core_csv, $cols);
}

function check_uv_id($resp_csv){
    $cols = array('id', 'groupe supérieur', 'nom', 'responsable', 'étudiant');
    $cols_missing = array('id_resp' => array(), 'nb_line' => array());
    $nb_line = 0;
    while(($resp_data = fgetcsv($resp_csv, 10000, ',')) !== FALSE){
        $id_resp = trim($resp_data[0]);
        if(!in_array($id_resp, $cols)){
            array_push($cols_missing['id_resp'], $id_resp);
            array_push($cols_missing['nb_line'], $nb_line);
        }
        $nb_line++;
    }
    if(count($cols_missing['id_resp']) > 0){
        return $cols_missing;
    }else{
        return false;
    }
}

function check_grp_col($grp_csv){
    $cols = array('id', 'nom');
    return check_col_exist($grp_csv, $cols);
}