<?php

function readCsvScore($scoreFile) {
    // index in csv file
    $iFirstNameStudent = -1;
    $iLastNameStudent = -1;
    $iMailStudent = -1;
    $iMailRespStud = -1;
    $iScoreTest = -1;
    $withRespStud = true;
    // array with data
    $firstNameStud = array();
    $lastNameStud = array();
    $mailRespStud = array();
    $mailStud = array();
    $scoresTest = array();
    $rowStud = 1;
    while (($dataStudent = fgetcsv($scoreFile, 10000, ',')) !== FALSE) {
        if ($rowStud == 1) {
            // on stocke le header du fichier
            $headerStudent = $dataStudent;
            // puis on cherche la position dans le header du nom, prénoms, mails, Référent
            $iFirstNameStudent = array_search('Prénom', $headerStudent);
            $iLastNameStudent = array_search('Nom', $headerStudent);
            $iMailRespStud = array_search('Tuteur', $headerStudent);
            $iScoreTest = array_search('Total du cours', $headerStudent);
            if (!$iMailRespStud) {
                $withRespStud = FALSE;
            }
            $iMailStudent = array_search('Adresse de courriel', $headerStudent);
            if (!$iMailStudent) {
                $iMailStudent = array_search('Adresse.de.courriel', $headerStudent);
            }
        } else {
            // pour chaque etudiants on récupère les informations utiles
            array_push($firstNameStud, $dataStudent[$iFirstNameStudent]);
            array_push($lastNameStud, $dataStudent[$iLastNameStudent]);
            if ($withRespStud) {
                array_push($mailRespStud, $dataStudent[$iMailRespStud]);
            }
            array_push($mailStud, $dataStudent[$iMailStudent]);
            array_push($scoresTest, $dataStudent[$iScoreTest]);
        }
        $rowStud++;
    }
    // enfin on ajoute ces information dans des varaible de session
    $formatData = array();
    $formatData['firstNameStud'] = $firstNameStud;
    $formatData['lastNameStud'] = $lastNameStud;
    $formatData['mailRespStud'] = $mailRespStud;
    $formatData['uniqueMailRespStud'] = array_unique($mailRespStud);
    $formatData['mailStud'] = $mailStud;
    $formatData['scoresTest'] = $scoresTest;
    $formatData['withRespStud'] = $withRespStud;
    return $formatData;
}