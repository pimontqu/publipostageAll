<?php

function readCsvResp($repsFile) {
    $idUEResp = null;
    $idUEResp = array();
    $nameUEResp = array();
    $respUEResp = array();
    // pour chaque ligne dans le fichier responsable
    while (($dataResp = fgetcsv($repsFile, 10000, ',')) !== FALSE) {
        // si la valeur de la première colonne de la ligne courante est égale 'id'
        if ($dataResp[0] == 'id') {
            // on en l'enlève la valeur de la première colonne
            array_shift($dataResp);
            // puis on stock la ligne courante dans une variable
            $idUEResp = $dataResp;
            // si la valeur de la première colonne de la ligne courante est égale 'nom'
        } elseif ($dataResp[0] == 'nom') {
            // on en l'enlève la valeur de la première colonne
            array_shift($dataResp);
            // puis on stock la ligne courante dans une variable
            $listOfName = $dataResp;
            for ($i = 0; $i < count($listOfName); $i++) {
                $nameUEResp[$idUEResp[$i]] = $listOfName[$i];
            }
            // si la valeur de la première colonne de la ligne courante est égale 'responsable'
        } elseif ($dataResp[0] == 'responsable') {
            array_shift($dataResp);
            $listOfResp = $dataResp;
            for ($i = 0; $i < count($listOfResp); $i++) {
                $respUERespTmp[$idUEResp[$i]] = $listOfResp[$i];
            }
            array_push($respUEResp, $respUERespTmp);
        }
    }
    $formatData = array();
    $formatData['listIdTDnUE'] = $idUEResp;
    $formatData['listNameTDnUE'] = $nameUEResp;
    $formatData['listRespTDnUE'] = $respUEResp;
    return $formatData;
}