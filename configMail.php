<?php
include_once 'config.php';
include_once  'controllers/configMailController.php';
?>
<!doctype html>
<html lang="fr">
    <head>
        <meta charset="UTF8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
        <title>publipostage</title>
    </head>
    <body class="container-fluid">
        <div class="row">
            <?php
            include_once 'assets/includeHtml/navBar.php';
            ?>
            <div class="col-sm-12 spaceUp">
                <h1>Configurations de l'envoi des courriels</h1>
                <?php
                if (isset($helpMessage) && isset($helpClass)) {
                    ?>
                    <p class="alert <?= $helpClass ?>" role="alert"><?= $helpMessage ?></p>
                    <?php
                }
                ?>
                <form action="configMail.php" method="POST">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="nameSend">Nom de l'expéditeur : *</label>
                            <input class="form-control" type="text" name="nameSend" id="nameSend" value="<?= $nameSend ?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="mailSend">Adresse courriel de l'expéditeur : *</label>
                            <input class="form-control" type="email" name="mailSend" id="mailSend" value="<?= $mailSend ?>" aria-describedby="mailSendHelp" required/>
                            <small id="mailSendHelp"><?= $newMailSendMessage ?></small>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="serverName"> Adresse du serveur d'envoi de courriel : *</label>
                            <input class="form-control" type="text" name="serverName" id="serverName" value="<?= $nameSendServer ?>" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="serverPort">Port du serveur d'envoi de courriel : *</label>
                            <input class="form-control" type="number" name="serverPort" id="serverPort" value="<?= $portSendServer ?>" aria-describedby="serverPortHelp" required/>
                            <small id="serverPortHelp"><?= $newPortSendServerMessage ?></small>
                        </div>
                    </div>
                    <div class="form-group list-inline">
                        <div class="col-sm-12">
                            <p>Serveur SMTP avec authentification : </p>
                            <ul class="list-unstyled">
                                <li class="list-inline-item">
                                    <label for="isAuth">Oui</label>
                                    <input type="radio" name="Auth" id="isAuth" value="isAuth" <?= ($isAuth) ? 'checked' : '' ?>/>
                                </li>
                                <li class="list-inline-item">
                                    <label for="notAuth">Non</label>
                                    <input type="radio" name="Auth" id="notAuth" value="notAuth" <?= (!$isAuth) ? 'checked' : '' ?>/>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="form-group list-inline" id="divSecurity">
                        <div class="col-sm-12">
                            <p>Type de Securité pour l'authentification : </p>
                            <ul class="list-unstyled">
                                <li class="list-inline-item">
                                    <label for="tls">TLS</label>
                                    <input type="radio" name="securityType" id="tls" value="tls" <?= ($securityType == 'tls') ? 'checked' : '' ?>/>
                                </li>
                                <li class="list-inline-item">
                                    <label for="ssl">SSL</label>
                                    <input type="radio" name="securityType" id="ssl" value="ssl" <?= ($securityType == 'ssl') ? 'checked' : '' ?>/>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="form-group" id="divUsername">
                        <div class="col-sm-12">
                            <label for="serverPort">identifiant/email pour l'authentification au serveur SMTP : </label>
                            <input class="form-control" type="text" name="username" id="username" value="<?= $usernameSmtp ?>"/>
                        </div>
                    </div>
                    <div class="form-group" id="divPassword">
                        <div class="col-sm-12">
                            <label for="serverPort">Mot de passe pour l'authentification au serveur SMTP : </label>
                            <input class="form-control" type="password" name="password" id="password" value="<?= $passwordSmtp ?>"/>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <input type="submit" name="updateConfigMail" class="btn btn-primary" value="Enregistrer"/>
                    </div>
                </form>
            </div>
        </div>
        <script src="assets/lib/jquery/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="assets/lib/popper/popper.min.js" type="text/javascript"></script>
        <script src="assets/lib/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/js/configMail.js" type="text/javascript"></script>
    </body>
</html>

