<?php
?>
<!doctype html>
<html lang="fr">
    <head>
        <meta charset="UTF8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
        <title>publipostage</title>
    </head>
    <body class="container-fluid">
        <div class="row">
            <?php
            include_once 'assets/includeHtml/navBar.php';
            ?>
            <div class="col-sm-12 spaceUp">
                L’outil « Publipostage » comprend 4 onglets (en plus de celui-ci) qui vont vous permettre
                <ul class="spaceUp">
                    <li>
                        de paramétrer l’édition de vos documents et de vous courriels : « Edition des entêtes », « Configuration de l’envoi des courriels », « Edition des courriels »
                    </li>
                    <li>
                        de générer vos documents à partir de trois fichiers à implémenter : le carnet de notes, la liste des groupes, la liste des thèmes disciplinaires testés.
                    </li>
                </ul>
                <p class="spaceUp">Pour plus d’information, vous pouvez consulter le guide « <a href="documentation.pdf" target="_blank">documentation.pdf</a> » disponible sur le bureau de la machine virtuelle.</p>
            </div>
        </div>
</html>