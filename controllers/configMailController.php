<?php
session_start();
// nom de l'éxpediteur
$nameSend = NAME_SEND;
// mails de l'expediteur
$mailSend = MAIL_SEND;
// addresse du serveur smtp
$nameSendServer = MAILSERVER_NAME;
// port du serveur smtp
$portSendServer = MAILSERVER_PORT;
// indique si le serveur smtp à besoin d'une connexion
$isAuth = (defined('IS_AUTH') ? IS_AUTH : 0);
// type de securité
$securityType = (defined('SECURITY') ? SECURITY : '');
// identifiant et mot de passe de connexion du serveur smtp
$usernameSmtp = (defined('USERNAME') ? USERNAME : '');
$passwordSmtp = (defined('PASSWORD') ? base64_decode(PASSWORD) : '');
// varible qui contiendra des message d'erreur ou informative pour l'utilisateur
$newPortSendServerMessage = '';
$newMailSendMessage = '';
// permettra d'inquer des erreur ou non.
$newPortSendServerError = FALSE;
$newMailSendError = FALSE;
if (isset($_POST['updateConfigMail'])) {
    // recupèration des champs de formulaires
    $mailSend = trim(strip_tags($_POST['mailSend']));
    $nameSend = trim(strip_tags($_POST['nameSend']));
    $nameSendServer = trim(strip_tags($_POST['serverName']));
    $portSendServer = trim(strip_tags($_POST['serverPort']));
    $isAuth = (trim(strip_tags($_POST['Auth'])) == 'isAuth' ? 1 : 0);
    $usernameSmtp = trim(strip_tags($_POST['username']));
    $passwordSmtp = base64_encode($_POST['password']);
    $securityType = trim(strip_tags($_POST['securityType']));
    // verifie si l'adresse mail et le port du server sont valide
    $newMailSendError = !preg_match('/^[a-z0-9._-]+@[a-z0-9._-]+\.[a-z]+$/i', $mailSend);
    $newPortSendServerError = !preg_match('/^[0-9]+$/i', $portSendServer);
    // définie les message d'erreur si il y en a dans le mail ou le port
    if ($newPortSendServerError) {
        $newPortSendServerMessage = 'Le port doit être un nomre entier';
    }
    if ($newMailSendError) {
        $newMailSendMessage = 'Entrez une adresse mail valide';
    }
    // si iln'y a pas d'erreur
    if (!$newMailSendError && !$newPortSendServerError) {
        // assigne le nouveau contenu du fichiers config.php
        $ContentConfig = '<?php' . PHP_EOL .
                'define(\'MAIL_SEND\', \'' . $mailSend . '\');' . PHP_EOL .
                'define(\'NAME_SEND\', \'' . $nameSend . '\');' . PHP_EOL .
                'define(\'MAILSERVER_NAME\', \'' . $nameSendServer . '\');' . PHP_EOL .
                'define(\'MAILSERVER_PORT\', ' . $portSendServer . ');' . PHP_EOL .
                'define(\'IS_AUTH\', ' . $isAuth . ');' . PHP_EOL .
                'define(\'USERNAME\', \'' . $usernameSmtp . '\');' . PHP_EOL .
                'define(\'PASSWORD\', \'' . $passwordSmtp . '\');' . PHP_EOL .
                'define(\'SECURITY\', \'' . $securityType . '\');' . PHP_EOL .
                'define(\'TDUE_SUBJECT\', \'' . escapeQuote(TDUE_SUBJECT) . '\');' . PHP_EOL .
                'define(\'TDUE_CONTENT\', <<<___HTML___' . PHP_EOL .
                TDUE_CONTENT . PHP_EOL .
                '___HTML___' . PHP_EOL .
                ');' . PHP_EOL .
                'define(\'STUDENT_SUBJECT\', \'' . escapeQuote(STUDENT_SUBJECT) . '\');' . PHP_EOL .
                'define(\'WEAK_STUDENT_CONTENT\', <<<___HTML___' . PHP_EOL .
                WEAK_STUDENT_CONTENT . PHP_EOL .
                '___HTML___' . PHP_EOL .
                ');' . PHP_EOL .
                'define(\'MEDIUM_STUDENT_CONTENT\', <<<___HTML___' . PHP_EOL .
                MEDIUM_STUDENT_CONTENT . PHP_EOL .
                '___HTML___' . PHP_EOL .
                ');' . PHP_EOL .
                'define(\'STRONG_STUDENT_CONTENT\', <<<___HTML___' . PHP_EOL .
                STRONG_STUDENT_CONTENT . PHP_EOL .
                '___HTML___' . PHP_EOL .
                ');' . PHP_EOL .
                'define(\'RESP_STUD_SUBJECT\', \'' . escapeQuote(RESP_STUD_SUBJECT) . '\');' . PHP_EOL .
                'define(\'RESP_STUD_CONTENT\', <<<___HTML___' . PHP_EOL .
                RESP_STUD_CONTENT . PHP_EOL .
                '___HTML___' . PHP_EOL .
                ');';
        // ouvre le fichier config.php
        $configFile = fopen('config.php', 'w+');
        // copie le contenue config.php dans un fichier temporaire configTemporary.php
        exec('cp config.php configTemporary.php');
        // si il n'y pas d'erreur lors de l'ecriture du fichier
        if (fwrite($configFile, $ContentConfig) != FALSE) {
            // on supprime le fichier temporaire configTemporary.php
            unlink('configTemporary.php');
            // on crée un message pour informé l'utilisateur que tout c'est bien passé
            $helpMessage = 'Les nouveaux paramètres ont bien été enregistrés';
            $helpClass = 'alert-success';            
            $passwordSmtp = base64_decode($passwordSmtp);
        } else {
            // copie le contenu de configTemporary.php dans config php
            exec('cp configTemporary.php config.php');
            // on supprime le fichier temporaire configTemporary.php
            unlink('configTemporary.php');
            // on crée un message pour informé l'utilisateur qu'e tout c'est bien passé'une erreur est survenue lors de la création 
            $helpMessage = 'Une erreur est est survenue lors de l\'enregistrements des paramètres. Veuillez ressayer.';
            $helpClass = 'alert-danger';
        }
    }
}
// échape les simple quote
function escapeQuote($text){
    $newText = str_replace('\'', '\\\'', $text);
    return $newText;
}