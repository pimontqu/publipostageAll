<?php
session_start();
// list des balise accepté par le strip tag
$tagAllowed = '<strong><lien><i><u>';
// les valeur des champs enttrée dans le fichiers
$subjectStud = (defined('STUDENT_SUBJECT') ?STUDENT_SUBJECT : 'Non renseigné');
$weakContentStud = (defined('WEAK_STUDENT_CONTENT') ? parsePlainText(WEAK_STUDENT_CONTENT, $tagAllowed) : 'Non renseigné');
$mediumContentStud = (defined('MEDIUM_STUDENT_CONTENT') ? parsePlainText(MEDIUM_STUDENT_CONTENT, $tagAllowed) : 'Non renseigné');
$strongContentStud = (defined('STRONG_STUDENT_CONTENT') ? parsePlainText(STRONG_STUDENT_CONTENT, $tagAllowed) : 'Non renseigné');
$subjectResp = (defined('TDUE_SUBJECT') ? TDUE_SUBJECT : 'Non renseigné');
$contentResp = (defined('TDUE_CONTENT') ? parsePlainText(TDUE_CONTENT, $tagAllowed) : 'Non renseigné');
$subjectRespStud = (defined('RESP_STUD_SUBJECT') ? RESP_STUD_SUBJECT : 'Non renseigné');
$contentRespStud = (defined('RESP_STUD_CONTENT') ? parsePlainText(RESP_STUD_CONTENT, $tagAllowed) : 'Non renseigné');
if (isset($_POST['updateContentMail'])) {
    // récupère, parse en html et échape les quotes des champs du formulaires
    $newSubjectStud = escapeQuote(trim(strip_tags($_POST['subjectStud'])));
    $newWeakContentStud = parseHtml(trim(strip_tags($_POST['weakContentStud'], $tagAllowed)));
    $newMediumContentStud = parseHtml(trim(strip_tags($_POST['mediumContentStud'], $tagAllowed)));
    $newStrongContentStud = parseHtml(trim(strip_tags($_POST['strongContentStud'], $tagAllowed)));
    $newSubjectResp = escapeQuote(trim(strip_tags($_POST['subjectResp'])));
    $newContentResp = parseHtml(trim(strip_tags($_POST['contentResp'], $tagAllowed)));
    $newSubjectRespStud = escapeQuote(trim(strip_tags($_POST['subjectRespStud'])));
    $newContentRespStud = parseHtml(trim(strip_tags($_POST['contentRespStud'], $tagAllowed)));
    // le nouveau contenu du fichier config.php
    $ContentConfig = '<?php' . PHP_EOL .
            'define(\'MAIL_SEND\', \'' . MAIL_SEND . '\');' . PHP_EOL .
            'define(\'NAME_SEND\', \'' . NAME_SEND . '\');' . PHP_EOL .
            'define(\'MAILSERVER_NAME\', \'' . MAILSERVER_NAME . '\');' . PHP_EOL .
            'define(\'MAILSERVER_PORT\', ' . MAILSERVER_PORT . ');' . PHP_EOL .
            'define(\'IS_AUTH\', ' . IS_AUTH . ');' . PHP_EOL .
            'define(\'USERNAME\', \'' . USERNAME . '\');' . PHP_EOL .
            'define(\'PASSWORD\', \'' . PASSWORD . '\');' . PHP_EOL .
            'define(\'SECURITY\', \'' . SECURITY . '\');' . PHP_EOL .
            'define(\'TDUE_SUBJECT\', \'' . $newSubjectResp . '\');' . PHP_EOL .
            'define(\'TDUE_CONTENT\', <<<___HTML___' . PHP_EOL .
            '<p>' . $newContentResp . '</p>' . PHP_EOL .
            '___HTML___' . PHP_EOL .
            ');' . PHP_EOL .
            'define(\'STUDENT_SUBJECT\', \'' . $newSubjectStud . '\');' . PHP_EOL .
            'define(\'WEAK_STUDENT_CONTENT\', <<<___HTML___' . PHP_EOL .
            '<p>' . $newWeakContentStud . '</p>' . PHP_EOL .
            '___HTML___' . PHP_EOL .
            ');' . PHP_EOL .
            'define(\'MEDIUM_STUDENT_CONTENT\', <<<___HTML___' . PHP_EOL .
            '<p>' . $newMediumContentStud . '</p>' . PHP_EOL .
            '___HTML___' . PHP_EOL .
            ');' . PHP_EOL .
            'define(\'STRONG_STUDENT_CONTENT\', <<<___HTML___' . PHP_EOL .
            '<p>' . $newStrongContentStud . '</p>' . PHP_EOL .
            '___HTML___' . PHP_EOL .
            ');' . PHP_EOL .
            'define(\'RESP_STUD_SUBJECT\', \'' . $newSubjectRespStud . '\');' . PHP_EOL .
            'define(\'RESP_STUD_CONTENT\', <<<___HTML___' . PHP_EOL .
            '<p>' . $newContentRespStud . '</p>' . PHP_EOL .
            '___HTML___' . PHP_EOL .
            ');';
    // ouvre le fichiers
    $configFile = fopen('config.php', 'w+');
    // copie le fichier danq un fichier temporaire configTemporary.php
    exec('cp config.php configTemporary.php');
    // met à jour les champs du formulaires
    $subjectStud = unescapQuote($newSubjectStud);
    $weakContentStud = parsePlainText($newWeakContentStud, $tagAllowed);
    $mediumContentStud = parsePlainText($newMediumContentStud, $tagAllowed);
    $strongContentStud = parsePlainText($newStrongContentStud, $tagAllowed);
    $subjectResp = unescapQuote($newSubjectResp);
    $contentResp = parsePlainText($newContentResp, $tagAllowed);
    $subjectRespStud = unescapQuote($newSubjectRespStud);
    $contentRespStud = parsePlainText($newContentRespStud, $tagAllowed);
    // si le nouveau contenue a bien été ecrits dans le fichier
    if (fwrite($configFile, $ContentConfig) != FALSE) {
        // supprime configTemporary.php
        unlink('configTemporary.php');
        // message d'indication pour l'utilisateur
        $helpMessage = 'L\'éditon des mails à bien été pris en compte';
        $helpClass = 'alert-success';
    } else {
        // copie configTemporary.php dans config.php
        exec('cp configTemporary.php config.php');
        // supprime configTemporary.php
        unlink('configTemporary.php');
        // message d'indication pour l'utilisateur
        $helpMessage = 'Une erreur est est survenue lors de l\'éditions des mails. Veuillez ressayer.';
        $helpClass = 'alert-danger';    
    }
}
// parse les champs en html
function parseHtml($input){
    $newValue = preg_replace('/<lien>(.*?)<\/lien>/i', '<a href="$1">$1</a>', $input);
    $newValue = str_replace(PHP_EOL, '</p><p>', $newValue);
    return $newValue;
}
// parse les champs en plaintext
function parsePlainText($text, $tagAllowed) {
    $newText = preg_replace('/<(\/){0,1}a.*?>/i', '<$1lien>', $text);
    $newText = strip_tags($newText, $tagAllowed);
    return $newText;
}
// échape les quotes 
function escapeQuote($text){
    $newText = str_replace('\'', '\\\'', $text);
    return $newText;
}
// déséchape les quotes 
function unescapQuote($text){
    $newText = str_replace('\\\'', '\'', $text);
    return $newText;
}