<?php
session_start();
// dossier content les fichier
$jsonPath = 'texteRapport/texteRapport.json';
// parse json file
$jsonFileContent = trim(file_get_contents($jsonPath));
$editableTextsForPdf = json_decode($jsonFileContent);
// variable qui indique s'il y a une erreur
$error = FALSE;
if (isset($_POST['updateHeader'])) {
    $newStudentFirstHeader = trim(strip_tags($_POST['student_first_header']));
    $newStudentSecondHeader = trim(strip_tags($_POST['student_second_header']));
    $newStudentMeanPrerequisTitle = trim(strip_tags($_POST['student_mean_prerequis_title']));
    $newStudentMeanRegistredUeTitle = trim(strip_tags($_POST['student_mean_registred_ue_title']));
    $newStudentDetailUeTitle = trim(strip_tags($_POST['student_detail_ue_title']));
    $newStudentMeanUnregistredUeTitle = trim(strip_tags($_POST['student_mean_unregistred_ue_title']));
    $newUeDocumentTitle = trim(strip_tags($_POST['ue_document_title']));
    $newUeHeader = trim(strip_tags($_POST['ue_header']));
    $newTdDocumentTitle = trim(strip_tags($_POST['td_document_title']));
    $newTdHeader = trim(strip_tags($_POST['td_header']));
    $newGlobalDocumentTitle = trim(strip_tags($_POST['global_document_title']));
    $newGlobalHeader = trim(strip_tags($_POST['global_header']));
    $newGlobalListPrerequisTitle = trim(strip_tags($_POST['global_list_prerequis_title']));
    // update la valeur du json
    $editableTextsForPdf->student->first_header = update_value($editableTextsForPdf->student->first_header, $newStudentFirstHeader);
    $editableTextsForPdf->student->second_header = update_value($editableTextsForPdf->student->second_header, $newStudentSecondHeader);
    $editableTextsForPdf->student->mean_prerequis_title = update_value($editableTextsForPdf->student->mean_prerequis_title, $newStudentMeanPrerequisTitle);
    $editableTextsForPdf->student->mean_registred_ue_title = update_value($editableTextsForPdf->student->mean_registred_ue_title, $newStudentMeanRegistredUeTitle);
    $editableTextsForPdf->student->detail_ue_title = update_value($editableTextsForPdf->student->detail_ue_title, $newStudentDetailUeTitle);
    $editableTextsForPdf->student->mean_unregistred_ue_title = update_value($editableTextsForPdf->student->mean_unregistred_ue_title, $newStudentMeanUnregistredUeTitle);
    $editableTextsForPdf->ue->document_title = update_value($editableTextsForPdf->ue->document_title, $newUeDocumentTitle);
    $editableTextsForPdf->ue->header = update_value($editableTextsForPdf->ue->header, $newUeHeader);
    $editableTextsForPdf->td->document_title = update_value($editableTextsForPdf->td->document_title, $newTdDocumentTitle);
    $editableTextsForPdf->td->header = update_value($editableTextsForPdf->td->header, $newTdHeader);
    $editableTextsForPdf->global->document_title = update_value($editableTextsForPdf->global->document_title, $newGlobalDocumentTitle);
    $editableTextsForPdf->global->header = update_value($editableTextsForPdf->global->header, $newGlobalHeader);
    $editableTextsForPdf->global->list_prerequis_title = update_value($editableTextsForPdf->global->list_prerequis_title, $newGlobalListPrerequisTitle);
    
    $headerLogo = $_FILES['logo_file'];
    if(!$headerLogo['error']){
        if($headerLogo['type'] == 'image/png' || $headerLogo['type'] == 'image/jpeg'){
            if(!move_uploaded_file($headerLogo['tmp_name'], 'appR/assets/images/logo.png')) {
                $error = TRUE;
            }
        } else {
            $error = TRUE;
        }
    } else {
        if($headerLogo['error'] != 4) {
            $error = TRUE;
        }
    }
    // update le fichier json avec la gestion d'erreur
    $jsonFile = fopen($jsonPath, 'w+');
    if(!fputs($jsonFile, json_encode($editableTextsForPdf) . PHP_EOL)) {
        $error = TRUE;
    }

    if($error) {
        $helpMessage = 'Une erreur est survenue lors de l\'éditons des entête veuillez ressayer';
        $helpClass = 'alert-danger';
    } else {
        $helpClass = 'alert-success';
        $helpMessage = 'L\'éditons des entête a bien été enregistré';
    }
}
// check si la nouvelle valeur est differente et l'update si besoin
function update_value($value, $newvalue){
    if($value != $newvalue) {
        return $newvalue;
    } else {
        return $value;
    }
}