<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';
session_start();
// permet d'exécuter le fichier a partir d'un terminal en utf-8
$locale = 'fr_FR.UTF-8';
setlocale(LC_ALL, $locale);
putenv('LC_ALL=' . $locale);
// dossier où seront stockés les csv
$dirCsv = 'appR/assets/csv/';
putenv('LC_ALL=' . $locale);
// dossier où seront stockés les rapport
$dirReport = 'appR/assets/rapport/';
$dirReportGlobal = scandir($dirReport . 'rapportGlobale/');
$dirReportStud = scandir($dirReport . 'rapportEtudiant/');
// indique si les rapports on etait créés
$generate = FALSE;
if(isset($_GET['sendMailResp']) || isset($_GET['sendMailStud']) || isset($_GET['sendMailRespStud']) || isset($_GET['sendAll'])){
    if (isset($_GET['sendMailResp']) && array_key_exists('listIdTDnUE', $_SESSION) && $_SESSION['withResp']) {
        if (array_key_exists('listIdTDnUE', $_SESSION)) {
    // si le bouton envoie mails resp est appuyé alors on envoie les mails a tous les responsables
            $responseResp = sendMailResp($_SESSION['listIdTDnUE'], $_SESSION['listRespTDnUE'], MAIL_SEND, NAME_SEND, TDUE_SUBJECT, MAILSERVER_NAME, MAILSERVER_PORT, TDUE_CONTENT);
        }
    } elseif (isset($_GET['sendMailStud']) && array_key_exists('mailStud', $_SESSION)) {
        if (array_key_exists('mailStud', $_SESSION)) {
            // si le bouton envoie mails étudiants est appuyé alors on envoie les mails a tous les étudiants
            $responseStud = sendMailStud($_SESSION['mailStud'], $_SESSION['firstNameStud'], $_SESSION['lastNameStud'], MAIL_SEND, NAME_SEND, STUDENT_SUBJECT, MAILSERVER_NAME, MAILSERVER_PORT, $_SESSION['scoresTest']);
        }
    } elseif (isset($_GET['sendMailRespStud'])) {
        if ($_SESSION['withRespStud']) {
            if (array_key_exists('uniqueMailRespStud', $_SESSION)) {
                // si le bouton envoie mails référant est appuyé alors on envoie les mails a tous les référants
                $responseRespStud = sendMailRespStud($_SESSION['uniqueMailRespStud'], $_SESSION['mailRespStud'], $_SESSION['firstNameStud'], $_SESSION['lastNameStud'], MAIL_SEND, NAME_SEND, RESP_STUD_SUBJECT, MAILSERVER_NAME, MAILSERVER_PORT, RESP_STUD_CONTENT);
            }
        }
    } elseif (isset($_GET['sendAll'])) {
        // si le bouton envoye tous les mails est appuyé
        if (array_key_exists('uniqueMailRespStud', $_SESSION)) {
            if ($_SESSION['withResp']) {
                // si la génération s'est faite avec le fichiers résponsable alors on envoie tous mails aux responsable
                $responseAllResp = sendMailResp($_SESSION['listIdTDnUE'], $_SESSION['listRespTDnUE'], MAIL_SEND, NAME_SEND, TDUE_SUBJECT, MAILSERVER_NAME, MAILSERVER_PORT, TDUE_CONTENT);
                if ($responseAllResp['error']) {
                    // indique s'il y a une erreur lors de l'envoie des mails aux responsables
                    $feedBackResp = 'Une Erreur est survenue lors de l\'envoi des mails aux responsables d\'UE et TD';
                    $classPResp = 'alert alert-danger';
                } else {
                    // indique s'il n'y a pas d'erreur lors de l'envoie des mails aux responsables
                    $feedBackResp = 'Les mails ont bien été envoyés aux responsables d\'UE et TD';
                    $classPResp = 'alert alert-success';
                }
            }
            if ($_SESSION['withRespStud']) {
                // envoie tous les mails aux tuteurs
                $responseAllRespStud = sendMailRespStud($_SESSION['uniqueMailRespStud'], $_SESSION['mailRespStud'], $_SESSION['firstNameStud'], $_SESSION['lastNameStud'], MAIL_SEND, NAME_SEND, RESP_STUD_SUBJECT, MAILSERVER_NAME, MAILSERVER_PORT, RESP_STUD_CONTENT);
                if ($responseAllRespStud['error']) {
                    // indique s'il y a une erreur lors de l'envoie des mails aux tuteurs
                    $feedBackRespStud = 'Une Erreur est survenue lors de l\'envoi des mails aux tuteurs';
                    $classPRespStud = 'alert alert-danger';
                } else {
                    // indique s'il n'y a pas d'erreur lors de l'envoie des mails aux tuteurs
                    $feedBackRespStud = 'Les mails ont bien été envoyés aux tuteurs';
                    $classPRespStud = 'alert alert-success';
                }
            }
            // envoie tous les mails aux étudiants
            $responseAllStud = sendMailStud($_SESSION['mailStud'], $_SESSION['firstNameStud'], $_SESSION['lastNameStud'], MAIL_SEND, NAME_SEND, STUDENT_SUBJECT, MAILSERVER_NAME, MAILSERVER_PORT, $_SESSION['scoresTest']);
            if ($responseAllStud['error']) {
                // indique s'il y a une erreur lors de l'envoie des mails aux étudiants
                $feedBackStud = 'Une Erreur est survenue lors de l\'envoi des mails aux étudiants';
                $classPStud = 'alert alert-danger';
            } else {
                // indique s'il n'y a pas d'erreur lors de l'envoie des mails aux étudiants
                $feedBackStud = 'Les mails ont bien été envoyés aux étudiants';
                $classPStud = 'alert alert-success';
            }
        }
    }
} else {
    // tableaux qui stockeront les info des etudiants, celles des responsables et des référents
    $firstNameStud = array();
    $lastNameStud = array();
    $mailStud = array();
    $scoresTest = array();
    $mailRespStud = array();
    $idUEResp = array();
    $nameUEResp = array();
    $respUEResp = array();
    if (isset($_POST['generate'])) {
        // supprime les fichiers csv
        deleteFileInDir($dirCsv);
        // récupère les fichiers csv uploader par l'utilisateur
        $scoreFileUpload = $_FILES['scoreFile'];
        $grpQFileUpload = $_FILES['grpQFile'];
        if (!empty($_FILES['respFile']['name'])) {
            $repsFileUpload = $_FILES['respFile'];
            $withResp = TRUE;
        } else {
            $withResp = FALSE;
        }
        if(array_key_exists('lastGen', $_POST)){
            $lastGen = true;
        }else{
            $lastGen = false;
        }
        
        // verifie pour tous les fichiers s'ils sont bien des csv si c'est le cas alors les sauvegarde dans le dossier csv. Sinon indique qu'il y une erreur
        // fichier de score
        if ($scoreFileUpload['type'] != 'text/csv') {
            $errorScoreFile = TRUE;
        } else {
            $errorScoreFile = FALSE;
            move_uploaded_file($scoreFileUpload['tmp_name'], $dirCsv . 'notes.csv');
        }
        // fichier des responsables
        if ($withResp) {
            if ($repsFileUpload['type'] != 'text/csv') {
                $errorRespFile = TRUE;
            } else {
                $errorRespFile = FALSE;
                move_uploaded_file($repsFileUpload['tmp_name'], $dirCsv . 'responsable.csv');
            }
        } else {
            $errorRespFile = FALSE;
        }
        // fichier des groupes de question
        if ($grpQFileUpload['type'] != 'text/csv') {
            $errorGrpQFile = TRUE;
        } else {
            $errorGrpQFile = FALSE;
            move_uploaded_file($grpQFileUpload['tmp_name'], $dirCsv . 'groupeQuestion.csv');
        }
        // s'il n'y a pas d'erreur dans le format des fichier
        if (!$errorScoreFile && !$errorGrpQFile && !$errorRespFile) {
            //verifie s'il n'y a pas d'erreur dans les liens de fichier
            $score_file = fopen($dirCsv . 'notes.csv', 'r');
            $grp_file = fopen($dirCsv . 'groupeQuestion.csv', 'r');
            if ($withResp) {
                $resp_file = fopen($dirCsv . 'responsable.csv', 'r');
                $link_ref_csv = get_typed_question($score_file, $grp_file, $resp_file);
                $error_uv = check_uv($link_ref_csv);
                $id_error_uv = check_uv_id($resp_file);
                fclose($resp_file);
            }else{
                $link_ref_csv = get_typed_question($score_file, $grp_file);
                $error_uv = false;
                $id_error_uv = false;
            }
            $error_grp = check_group_question($link_ref_csv);
            fclose($score_file);
            fclose($grp_file);
            $score_file = fopen($dirCsv . 'notes.csv', 'r');
            $grp_file = fopen($dirCsv . 'groupeQuestion.csv', 'r');
            $col_error_score = check_score_col($score_file);
            $col_error_grp = check_grp_col($grp_file);
            fclose($score_file);
            fclose($grp_file);
            if(!$error_uv && !$error_grp && !$col_error_score && !$id_error_uv && !$col_error_grp){
                if (!$lastGen) {
                    // supprime tous les fichiers pdf(rapports)
                    deleteFileInSubDir($dirReport);
                    //exécute le script R qui créer les rapports en pdf
                    $descriptorspec = array(
                        0 => array('pipe', 'r'), // stdin is a pipe that the child will read from
                        1 => array('pipe', 'w'), // stdout is a pipe that the child will write to
                        2 => array('pipe', "w")    // stderr is a pipe that the child will write to
                    );
                    flush();
                    //exécute le script R qui créer les rapports en pdf et l'affiche l'anvancé dans l'ecran
                    $process = proc_open('Rscript appR/script.R 2>&1', $descriptorspec, $pipes);
                    echo '<div id="logsContainer" style="height: 200px; overflow-y: scroll;border : 2px solid black"> <pre class="log">';
                    if (is_resource($process)) {
                        while ($s = fgets($pipes[1])) {
                            print $s;
                            flush();
                        }
                    }
                    echo '</pre></div>';
                } else {
                    touch($dirCsv . 'notError.txt');
                }
                // si le script R s'est deroulé sans erreur
                if (file_exists($dirCsv . 'notError.txt')) {
                    // on ouvre le fichier des responsables ainsi que celui des etudiants et leurs notes
                    $scoreFile = fopen($dirCsv . 'notes.csv', 'r');
                    if ($scoreFile === FALSE) {
                        errormsg("erreur interne : impossible d'ouvrir le fichier de note des étudiants (notes.cvs)");
                    }
                    if ($withResp) {
                        $repsFile = fopen($dirCsv . 'responsable.csv', 'r');
                        $_SESSION['withResp'] = TRUE;
                        if ($scoreFile === FALSE) {
                            errormsg("erreur interne : impossible d'ouvrir le fichier de groupes (responsable.csv)");
                        }
                        // lit le csv des uv/td
                        $dataResp = readCsvResp($repsFile);
                        // on ajoute tous les valeurs stocké dans des variable de sessions
                        $_SESSION['listIdTDnUE'] = $dataResp['listIdTDnUE'];
                        $_SESSION['listNameTDnUE'] = $dataResp['listNameTDnUE'];
                        $_SESSION['listRespTDnUE'] = $dataResp['listRespTDnUE'];
                    } else {
                        $_SESSION['withResp'] = FALSE;
                    }
                    // lit le csv des notes
                    $dataStudent = readCsvScore($scoreFile);
                    // enfin on ajoute ces information dans des varaible de session
                    $_SESSION['firstNameStud'] = $dataStudent['firstNameStud'];
                    $_SESSION['lastNameStud'] = $dataStudent['lastNameStud'];
                    $_SESSION['mailRespStud'] = $dataStudent['mailRespStud'];
                    $_SESSION['uniqueMailRespStud'] = array_unique($dataStudent['mailRespStud']);
                    $_SESSION['mailStud'] = $dataStudent['mailStud'];
                    $_SESSION['scoresTest'] = $dataStudent['scoresTest'];
                    $_SESSION['withRespStud']= $dataStudent['withRespStud'];
                    
                    // zip tous les rapports pour les téléchargez les raport
                    $linkZipFile = zipAllReport($dirReport);
                    // et on idinque que les rapports on bien été créés.
                    $generate = TRUE;
                } else {
                    // sinon on supprime tous les fichiers enregistrés du dossier
                    deleteFileInDir($dirCsv);
                }
            }else{
                $error_link_ref = true;
                deleteFileInDir($dirCsv);
            }
        } else {
            // s'il y a des erreurs dans le format on supprime tous les fichiers enregistrés du dossier
            deleteFileInDir($dirCsv);
        }
    }
}

function errormsg($msg) {
    echo '<div class="errormsg">' . $msg . '</div>';
    die();
}

function warnmsg($msg) {
    echo '<div class="warnmsg">' . $msg . '</div>';
}

// initialise les paramètres communs à tous les mails
function initMail($from, $fromName, $subject, $host, $port) {
    $sendMail = new PHPMailer(TRUE);
    // on précise l'encodage utf-8
    $sendMail->CharSet = 'UTF-8';
    // on précise le mail de l'expediteur
    $sendMail->From = $from;
    // on précise le nom de l'expediteur
    $sendMail->FromName = $fromName;
    // on renseigne le sujet du mail
    $sendMail->Subject = $subject;
    $sendMail->IsSMTP();
    $sendMail->Host = $host;
    $sendMail->Port = $port;
    // si le serveur a besoin d'une connexion on renseigne les identifiant
    if(IS_AUTH){
        // indique qu'il une identification login/mot de passe a ce serveur
        $sendMail->SMTPAuth = TRUE;
        // le type de security (ssl ou tls)
        $sendMail->SMTPSecure = SECURITY;
        // login et mot de passe pour la connexion
        $sendMail->Username = USERNAME;
        $sendMail->Password = base64_decode(PASSWORD);
    }
    return $sendMail;
}

// supprime les adresse mails et pièces jointes aux mails
function DeleteContentMail(PHPMailer $mail) {
    $mail->clearAddresses();
    $mail->clearAttachments();
}

// supprime tous les fichiers d'un dossier
function deleteFileInDir($dir) {
    // récupère tous les fichiers et dossier à l'interieur du dossier
    $scanDir = scandir($dir);
    // pour chaque élément dans le dossier
    foreach ($scanDir as $file) {
        // si cet élément est un fichier alors on le supprime.
        if (is_file($dir . $file) && $file != '.notempty') {
            unlink($dir . $file);
        }
    }
}

// supprime les dossier et les fichier du dossier rapportTDUE
function deleteTDUEContent($dir) {
    $contentDir = scandir($dir);
    foreach ($contentDir as $content) {
        if (is_dir($dir . $content) && $content !== '.' && $content !== '..') {
            deleteFileInDir($dir . $content . '/');
            rmdir($dir . $content . '/');
        } else {
            if ($content !== '.' && $content !== '..' && $content !== '.notempty') {
                unlink($dir . $content);
            }
        }
    }
}

// supprime tous les fichiers d'un sous dossier
function deleteFileInSubDir($dir) {
    // on recupère tous les dossiers est fichiers du dossier
    $scanDir = scandir($dir);
    // pour chaque éléments 
    foreach ($scanDir as $element) {
        if (is_dir($dir . $element) && $element !== '.' && $element !== '..' && $element !== 'rapportTDUE') {
            // si l'éléments est un dossier on supprimer tous les fichiers a l'intèrieur 
            deleteFileInDir($dir . $element . '/');
        } elseif (is_dir($dir . $element) && $element == 'rapportTDUE') {
            deleteTDUEContent($dir . $element . '/');
        }
    }
}
// zip les fichiers supplémentaire pour les responsable UE/TD
function zipFileResp($dir, $nameZipFile){
    $pathZip = $dir . $nameZipFile . '.zip';
    $zip = new ZipArchive();
    $zip->open($pathZip, ZipArchive::CREATE);
    $scan = scandir($dir);
    foreach($scan as $file){
        if($file !== '..' && $file !== '.' && $file !== $nameZipFile . '.zip'){
            $pathCurrentFile = $dir . $file;
            $zip->addFile($pathCurrentFile, $file);
        }
    }
    $zip->close();
    return $pathZip;
}
//zip all rapport
function zipAllReport($reportDir){
    $nameZipFile = 'rapports.zip';
    $pathZip = $reportDir . $nameZipFile;
    $zip = new ZipArchive();
    if(is_file($pathZip)){
        $zip->open($pathZip, ZipArchive::OVERWRITE);
    }else {
        $zip->open($pathZip, ZipArchive::CREATE);
    }
    $scan = scandir($reportDir);
    $assosNameFolder = array(
        'rapportEtudiant' => 'rapports étudiants',
        'rapportGlobale' => 'rapport globale',
        'rapportTDUE' => 'rapports UE TD',
    );
    foreach($scan as $folder){
        if(is_dir($reportDir . $folder) && $folder !== '..' && $folder !== '.' && $folder !== $nameZipFile){
            $subFolder = scandir($reportDir . $folder);
            $subFolderRewrite = $assosNameFolder[$folder];
            foreach($subFolder as $file){
                $pathCurrentFile = $reportDir . $folder . '/'. $file;
                if(is_file($pathCurrentFile) && $file !== '..' && $file !== '.' && $file != '.notempty'){
                    $zip->addFile($pathCurrentFile, $subFolderRewrite . '/'. $file);
                }
            }
        }
    }
    $zip->close();
    return $pathZip;

}
// envoie les mails aux résponsables UE/TD
function sendMailResp($idResp, $mailResp, $mailSend, $nameSend, $subject, $serverName, $serverPort, $mailContent) {
    $mailInvalid = array();
    //on initialise les paramètre du mails (l'adresse mails et son nom, le sujet et le le server smtp et son port)
    $sendMail = initMail($mailSend, $nameSend, $subject, $serverName, $serverPort);
    // chemin du fichier de logs
    $fileLog = 'logs/mailResponsable';
    $path = 'appR/assets/rapport/rapportTDUE/';
    for ($iResp = 0; $iResp < count($idResp); $iResp++) {
        $indexTDUE = $idResp[$iResp];
        DeleteContentMail($sendMail);
        // On définit le contenue du mail
        $sendMail->Body = '<p>Bonjour,</p>' . $mailContent;
        // Precise que le contenue est ecrit en html
        $sendMail->isHTML(TRUE);
        // On ajoute le ou les destinataires
        foreach ($mailResp as $listMailSend) {
            $currentMailSend = trim($listMailSend[$indexTDUE]);
            if (!empty(trim($currentMailSend))) {
                if ($sendMail->validateAddress($currentMailSend)) {
                    $sendMail->addAddress($currentMailSend);
                    //$sendMail->addAddress('quentin.pimont@utc.fr');
                } else {
                    array_push($mailInvalid, $currentMailSend);
                }
            }
        }
        if (count($sendMail->getAllRecipientAddresses()) != 0) {
            // on recupère le chemein du rapport qui correspond à l'étudiant
            $pathFile = $path . str_replace(array(' ', '-'), '.', $indexTDUE) . '.pdf';
            // chemin où sont enregistré les fichier exporté (fichier excel avec les note, graphique, etc)
            $dirExportFile = $path . str_replace(array(' ', '-'), '.', $indexTDUE) . '/';
            // zip les fichiers exporter et retourne le chemin du fichier
            $zipFile = zipFileResp($dirExportFile, $indexTDUE);
            // On ajoute le rapport en piece jointe et le fichier zip
            $sendMail->addAttachment($pathFile, 'rapportTestPositionnement.pdf');
            $sendMail->addAttachment($zipFile);
            if (!$sendMail->send()) {
                // on indique qu'il y a une erreur et sors de la boucle
                $feedBackResp = 'erreur envoie mails';
                $classPResp = 'alert alert-danger';
                $errorResp = TRUE;
                break;
            } else {
                // on ecrit dans un fichier de logs le mails a bien été envoyé
                file_put_contents($fileLog . '-' . date('d-m-Y') . '.log', 'le mail à bien été envoie à l\'adresse suivante : ' . $currentMailSend . PHP_EOL, FILE_APPEND);
                // on indique que tous les mails sont envoyé pour le momment
                $feedBackResp = 'Les mails ont bien été envoyé aux responsables';
                $classPResp = 'alert alert-success';
                $errorResp = FALSE;
            }
        }
    }
    return array('feedBack' => $feedBackResp, 'class' => $classPResp, 'error' => $errorResp, 'mailInvalid' => $mailInvalid);
}

// envoie les mails aux étudiants
function sendMailStud($mailStud, $firstName, $lastName, $mailSend, $nameSend, $subject, $serverName, $serverPort, $scoreTest) {
    $mailInvalid = array();
    //on initialise les paramètre du mails (l'adresse mails et son nom, le sujet et le le server smtp et son port)
    $sendMail = initMail($mailSend, $nameSend, $subject, $serverName, $serverPort);
    // chemin du fichier de logs
    $fileLog = 'logs/mailEtudiant';
    $path = 'appR/assets/rapport/rapportEtudiant/';
    for ($iStud = 0; $iStud < count($mailStud); $iStud++) {
        // email de l'étudiant courant
        $currentMailSendStud = trim($mailStud[$iStud]);
        if (!empty(trim($currentMailSendStud))) {
            if ($sendMail->validateAddress($currentMailSendStud)) {
                // supprime le contenu du mails précedant
                DeleteContentMail($sendMail);
                // sa note total au test
                $currentScore = $scoreTest[$iStud];
                // convertit sa note en float
                $currentScoreNumeric = parseNumeric($currentScore);
                // on définit le contenue du mails
                $sendMail->Body = '<p>Bonjour ' . $firstName[$iStud] . ' ' . $lastName[$iStud] . ',</p>' . getContentMailStuds($currentScoreNumeric);
                // Précise que le contenue est ecrit en html
                $sendMail->isHTML(TRUE);
                // On ajoute le déstinataire
                $sendMail->addAddress($currentMailSendStud);
                //$sendMail->addAddress('quentin.pimont@utc.fr');
                // on recupère le chemin du rapport qui correspond à l'étudiant
                $pathFile = $path . $firstName[$iStud] . '_' . $lastName[$iStud] . '.pdf';
                // On ajoute le rapport en piéce jointe
                $sendMail->addAttachment($pathFile, 'résultatTestPositionnement.pdf');
                if (!$sendMail->send()) {
                    // on indique qu'il y a une erreur et sors de la boucle
                    $feedBackStud = 'erreur envoie mails';
                    $classPStud = 'alert alert-danger';
                    $errorStud = TRUE;
                    break;
                } else {
                    // on ecrit dans un fichier de logs le mails a bien été envoyé
                    $logs = $fileLog . '-' . date('d-m-Y') . '.log';
                    file_put_contents($logs, 'le mail à bien été envoie à l\'adresse suivante : ' . $currentMailSendStud . PHP_EOL, FILE_APPEND);
                    // on indique que tous les mails sont envoyé pour le momment
                    $feedBackStud = 'Les mails ont bien été envoyé aux étudiants';
                    $classPStud = 'alert alert-success';
                    $errorStud = FALSE;
                }
            } else {
                array_push($mailInvalid, $currentMailSendStud);
            }
        }
    }
    return array('feedBack' => $feedBackStud, 'class' => $classPStud, 'error' => $errorStud, 'mailInvalid' => $mailInvalid);
}

// envoie les mails aux référents
function sendMailRespStud($uniqueMail, $mailRespStud, $firstNameStud, $lastNameStud, $mailSend, $nameSend, $subject, $serverName, $serverPort, $mailContent) {
    $mailInvalid = array();
    //on initialise les paramètre du mails (l'adresse mails et son nom, le sujet et le le server smtp et son port)
    $sendMail = initMail($mailSend, $nameSend, $subject, $serverName, $serverPort);
    // chemin du fichier de logs
    $fileLog = 'logs/mailRéférant';
    $path = 'appR/assets/rapport/rapportEtudiant/';
    foreach ($uniqueMail as $respStud) {
        $currentMail = trim($respStud);
        if (!empty(trim($currentMail))) {
            if ($sendMail->validateAddress($currentMail)) {
                DeleteContentMail($sendMail);
                $sendMail->Body = '<p>Bonjour,<p>' . $mailContent;
                $sendMail->isHTML(TRUE);
                $sendMail->addAddress($currentMail);
                for ($iRespStud = 0; $iRespStud < count($mailRespStud); $iRespStud++) {
                    if ($mailRespStud[$iRespStud] == $currentMail) {
                        // on recupère le chemin du rapport qui correspond à l'étudiant
                        $pathFile = $path . $firstNameStud[$iRespStud] . '_' . $lastNameStud[$iRespStud] . '.pdf';
                        $sendMail->addAttachment($pathFile);
                    }
                }
                if (!$sendMail->send()) {
                    // on indique qu'il y a une erreur et sors de la boucle
                    $feedBackRespStud = 'erreur envoie mails';
                    $classPRespStud = 'alert alert-danger';
                    $errorRespStud = TRUE;
                    break;
                } else {
                    // on ecrit dans un fichier de logs le mails a bien été envoyé
                    file_put_contents($fileLog . '-' . date('d-m-Y') . '.log', 'le mail à bien été envoie à l\'adresse suivante : ' . $currentMail . PHP_EOL, FILE_APPEND);
                    // on indique que tous les mails sont envoyé pour le momment
                    $feedBackRespStud = 'Les mails ont bien été envoyé aux tuteurs';
                    $classPRespStud = 'alert alert-success';
                    $errorRespStud = FALSE;
                }
            } else {
                array_push($mailInvalid, $currentMail);
            }
        }
    }
    return array('feedBack' => $feedBackRespStud, 'class' => $classPRespStud, 'error' => $errorRespStud, 'mailInvalid' => $mailInvalid);
}
// parse le score de l'étudiant en float
function parseNumeric($score){
    // la remplace la possible ',' en '.'
    $scoreDot = str_replace(',', '.', $score);
    // supprime le possible symole '%' 
    $scoreNumeric = (float)str_replace('%', '', $scoreDot);
    return $scoreNumeric;
}
// récupère le text pour le mails selon la note de l'étudiant
function getContentMailStuds($score){
    if($score < 50){
        $content = WEAK_STUDENT_CONTENT;
    }elseif($score > 75){
        $content = STRONG_STUDENT_CONTENT;
    }else{
        $content = MEDIUM_STUDENT_CONTENT;
    }
    return $content;
}