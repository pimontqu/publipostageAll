<?php

// on lance la session
session_start();
include_once '../utils/readCsvScores.php';
include_once '../utils/readCsvResp.php';
$csvDir = '../appR/assets/csv/';
$dirReport = '../appR/assets/rapport/';
// $dirReportGlobal = scandir($dirReport . 'rapportGlobale/');
// $dirReportStud = scandir($dirReport . 'rapportEtudiant/');
// si le bouton "afficher plus de résultat" pour le tableau des responsable est clicker
if (isset($_POST['appendTabResp'])) {
    // des tableaux qui contiendront les nouvelles entrées pour le tableau html!
    $newIds = array();
    $newMails = array();
    $newNames = array();
    // indique si le tableaux a atteint sa longueur maximum
    $maxLength = FALSE;
    $respFile = fopen($csvDir . 'responsable.csv', 'r');
    if ($respFile === FALSE) {
        errormsg("erreur interne : impossible d'ouvrir le fichier de groupes (responsable.csv)");
    }
    $dataResp = readCsvResp($respFile);
    // recupère la clé du dernier élément afficher dans le tableau html
    $keyLastElement = array_search($_POST['lastElement'], $dataResp['listNameTDnUE']);
    // puis on recherche la place de cet clé dans un tableaux qui les contient toutes qu'on ajoute 1 pour savoir à quel endroit commencer a recupèrer le nouvelle valuers 
    $countTab = array_search($keyLastElement, $dataResp['listIdTDnUE']) + 1;
    // represante la taille du tableaux html
    $count = $countTab;
    // boucle qui s'excutera quand que la taille sera inferieure à la taille du tableaux qui contient toutes les id ou inferieur a sa taille actuelle + 200
    for ($iAppendResp = $countTab; $iAppendResp < $countTab + 200 && $iAppendResp < count($dataResp['listIdTDnUE']); $iAppendResp++) {
        // on recupère l'id du td ou de l'ue courant
        $currentId = $dataResp['listIdTDnUE'][$iAppendResp];
        // parcour la liste des responsable (tableaux à deux dimmension)
        foreach ($dataResp['listRespTDnUE'] as $lisResp) {
            // on récupère l'a valeur'email du responsable courant grâce à l'id de l'ue/td qui ser de clé
            $currentResp = $lisResp[$currentId];
            if (!empty($currentResp)) {
                // si la valeur n'est pas vide on ajoute le valuer de ce responsable (email, nom et id de l'ue/td)a
                array_push($newIds, str_replace(array(' ', '-'), '.', $currentId));
                array_push($newMails, $currentResp);
                array_push($newNames, $dataResp['listNameTDnUE'][$currentId]);
            }
        }
        // puis on ajoute un a longueur du tableaux
        $count++;
    }
    if ($count == count($dataResp['listIdTDnUE'])) {
        // si la longueur du tableaux et égale la longueur tu tableaux d'id alors on indique que le tab html et a atteint sa valeur maximale
        $maxLength = TRUE;
    }
    // on envoie les infos aux javascript
    echo json_encode(array('newIds' => $newIds, 'newMails' => $newMails, 'newNames' => $newNames, 'maxLength' => $maxLength));
// si le bouton "afficher plus de résultat" pour le tableau des étudiant est clicker
} elseif (isset($_POST['tabStud'])) {
    // indique si le tableaux a atteint sa longueur maximum
    $maxLength = FALSE;
    // longueur du tableaux au moment du click
    $count = intval($_POST['countTab']);
    // tableaux qui contiendront les nouvelles entrées pour le tableau html
    $newNames = array();
    $newMails = array();
    $newRespStud = array();
    $newFile = array();
    $scoreFile = fopen($csvDir . 'notes.csv', 'r');
    if ($scoreFile === FALSE) {
        errormsg("erreur interne : impossible d'ouvrir le fichier de groupes (responsable.csv)");
    }
    $dataStudent = readCsvScore($scoreFile);
    // boucle qui s'excutera quand que la taille sera inferieure à la taille du tableaux qui contient tous les mails des étudiants ou inferieur a sa taille actuelle + 200
    for ($iStud = intval($_POST['countTab']); $iStud < intval($_POST['countTab']) + 200 && $iStud < count($dataStudent['mailStud']); $iStud++) {
        // on ajoute les valeurs au tableaux correspondante (nom, mail, référent et nom du fichier)
        array_push($newNames, $dataStudent['firstNameStud'][$iStud] . ' ' . $dataStudent['lastNameStud'][$iStud]);
        array_push($newMails, $dataStudent['mailStud'][$iStud]);
        array_push($newFile, $dataStudent['firstNameStud'][$iStud] . '_' . $dataStudent['lastNameStud'][$iStud] . '.pdf');
        if($dataStudent['withRespStud']) {
            array_push($newRespStud, $dataStudent['mailRespStud'][$iStud]);
        }
        // puis on ajoute un a longueur initiale du tableaux
        $count++;
    }
    if ($count == count($dataStudent['mailStud'])) {
        // si la longueur du tableaux et égale la longueur tu tableaux des mails alors on indique que le tab html et a atteint sa valeur maximale 
        $maxLength = TRUE;
    }
    // on envoie les infos aux javascript
    echo json_encode(array('newNames' => $newNames, 'newMails' => $newMails, 'newFiles' => $newFile, 'newRespStuds' => $newRespStud, 'withRespStud' => $dataStudent['withRespStud'], 'maxLength' => $maxLength));
} elseif (isset($_POST['lastElementTabResp'])) {
    if (key_exists('listIdTDnUE', $_SESSION)) {
        // on recherche la clé du dernier élément du tableau html
        $lastElement = array_search($_POST['lastElementTabResp'], $_SESSION['listNameTDnUE']);
        // on recherche la position de cette clé dans un tableau qui les contient tous et on lui ajoute un
        $countResp = array_search($lastElement, $_SESSION['listIdTDnUE']) + 1;
        // on récupère la taille du tableaux qui contient toutes les id td/ue
        $countArray = count($_SESSION['listIdTDnUE']);
        // on verifie si la position de la clé + 1 est égale la taille du tableau des ids
        $isFullResp = $countResp === count($_SESSION['listIdTDnUE']);
        //on envoie les résultats au javascripts
        echo json_encode(array('tabRespFull' => $isFullResp));
    } else {
}
} elseif(isset($_POST['countTabStud'])) {
    if (key_exists('mailStud', $_SESSION)) {
        // on regarde si la longueur du tableaux html est la même que celui contenant les mails des etudiants
        $isFullStud = intval($_POST['countTabStud']) === count($_SESSION['mailStud']);
        echo json_encode(array('tabStudFull' => $isFullStud));
    }
}