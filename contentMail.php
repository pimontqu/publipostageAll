<?php
include_once 'config.php';
include_once 'controllers/contentMailController.php';
?>
<!doctype html>
<html lang="fr">
    <head>
        <meta charset="UTF8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
        <link href="assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous"/>
        <link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
        <script src="assets/js/formatingTextArea.js" type="text/javascript"></script>
        <title>publipostage</title>
    </head>
    <body class="container-fluid">
        <div class="row">
            <?php
            include_once 'assets/includeHtml/navBar.php';
            ?>
            <div class="col-sm-12 spaceUp">
                <h1>Éditions des courriels</h1>
                <p>Ce formulaire permet d'éditer les objets et contenus des courriels envoyés.</p>
                <p class="warning"><i class="fas fa-exclamation-triangle"></i> Les courriels pour les responsables et les tuteurs/conseillers commencent par <strong>Bonjour,</strong> et ceux des étudiants par <strong>Bonjour "prénom et nom de l'étudiant"</strong>.</p>
                <?php
                if (isset($helpMessage) && isset($helpClass)) {
                    ?>
                    <p class="alert <?= $helpClass ?>" role="alert"><?= $helpMessage ?></p>
                    <?php
                }
                ?>
                <form action="contentMail.php" method="POST">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="subjectStud">Objet du courriel pour les étudiants : *</label>
                        </div>
                        <input class="form-control" type="text" name="subjectStud" id="subjectStud" value="<?= $subjectStud ?>" required>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="contentStud">Contenu du courriel pour les étudiants dits de faible niveau (< 50%) : *</label>
                        </div>
                        <div class="formatingEditor">
                            <?php include 'assets/includeHtml/btnEditionText.php' ?>
                            <textarea class="form-control noRadiusTop" name="weakContentStud" id="weakContentStud" required><?= $weakContentStud ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="contentStud">Contenu du courriel pour les étudiants dits de niveau moyen (entre 50% et 75%) : *</label>
                        </div>
                        <div class="formatingEditor">
                            <?php include 'assets/includeHtml/btnEditionText.php' ?>
                            <textarea class="form-control noRadiusTop" name="mediumContentStud" id="mediumContentStud" required><?= $mediumContentStud ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="contentStud">Contenu du courriel pour les étudiants dits de niveau fort (> 75 %) : *</label>
                        </div>
                        <div class="formatingEditor">
                            <?php include 'assets/includeHtml/btnEditionText.php' ?>
                            <textarea class="form-control noRadiusTop" name="strongContentStud" id="strongContentStud" required><?= $strongContentStud ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="subjectResp">Objet du courriel pour les responsables d'UE et TD/TP : *</label>
                        </div>
                        <input class="form-control" type="" name="subjectResp" id="subjectResp" value="<?= $subjectResp ?>" required>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="contentResp">Contenu du courriel pour les responsables d'UE et TD/TP : *</label>
                        </div>
                        <div class="formatingEditor">
                            <?php include 'assets/includeHtml/btnEditionText.php' ?>
                            <textarea class="form-control noRadiusTop" name="contentResp" id="contentResp" required=" required"><?= $contentResp ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="subjectRespStud">Objet du courriel pour les tuteurs/conseillers : *</label>
                        </div>
                        <input class="form-control" type="text" name="subjectRespStud" id="subjectRespStud" value="<?= $subjectRespStud ?>" required>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="contentRespStud">Contenu du courriel pour les tuteurs/conseillers : *</label>
                        </div>
                        <div class="formatingEditor">
                            <?php include 'assets/includeHtml/btnEditionText.php' ?>
                            <textarea class="form-control noRadiusTop" name="contentRespStud" id="contentRespStud" required><?= $contentRespStud ?></textarea>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <input type="submit" name="updateContentMail" class="btn btn-primary" value="Enregistrer"/>
                    </div>
                </form>
            </div>
        </div>
        <script src="assets/lib/jquery/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="assets/lib/popper/popper.min.js" type="text/javascript"></script>
        <script src="assets/lib/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>