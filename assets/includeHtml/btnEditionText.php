<div class="btn-group btn-group-sm textEdit" role="group">
    <button type="button" class="btn btn-primary noRadiusBottom" onclick="makeInlineTag(this, 'strong')"><i class="fas fa-bold"></i></button>
    <button type="button" class="btn btn-primary noRadiusBottom" onclick="makeInlineTag(this, 'i')"><i class="fas fa-italic"></i></button>
    <button type="button" class="btn btn-primary noRadiusBottom" onclick="makeInlineTag(this, 'u')"><i class="fas fa-underline"></i></button>
    <button type="button" class="btn btn-primary noRadiusBottom" onclick="makeInlineTag(this, 'lien')"><i class="fas fa-link"></i></button>
</div>