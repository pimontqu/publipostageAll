// ajoute des balise inline dans le texte area (ex : b, i, etc)
var makeInlineTag = function (btn, tag) {
    // récupère le parent du parent du bouton
    var parent = btn.parentNode.parentNode;
    // récupère le textearea qui le dernier enfant du parent
    var editor = parent.lastElementChild;
    // offset qui prend en compte les 2 deux crochet de la balise ouvrante ainsi que la longueur du nom de la balise
    var offsetFocus = 2 + tag.length;
    // récupère l'index du début et de la fin de la selection de l'utilisateur
    var start = editor.selectionStart;
    var end = editor.selectionEnd;
    // si l'utilisateur n'a pas fait de selection
    if(start === end){
        // alors on place les balises à l'endroit ou sont curseur se trouve
        editor.value = editor.value.slice(0, start) + '<' + tag + '>' + '</' + tag + '>' + editor.value.slice(end);
    }else{
        // sinon on place les balise au début et à la fin de la séléction
        editor.value = editor.value.slice(0, start) + '<' + tag + '>' + editor.value.slice(start, end) + '</' + tag + '>' + editor.value.slice(end);
    }
    // on redonne le focus à l'editeur de texte
    editor.focus();
    // puis on place la selection ou le curseur entre les nouvelles balises
    editor.selectionStart = start + offsetFocus;
    editor.selectionEnd = end + offsetFocus;
};