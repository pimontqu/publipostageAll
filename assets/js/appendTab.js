$(function () {
    if ($('#tabResp').length) {
        $.post('controllers/ajax.php', {
            // avec le dernier element du tableaux des responsable
            lastElementTabResp: document.getElementById('bodyTabResp').rows[document.getElementById('bodyTabResp').rows.length - 1].cells[0].innerHTML
        }, function (data) {
            // on récupère les infos renvoyé par le php
            var tabRespFull = data.tabRespFull;
            // puis les 2 tableaux on affiche ou non le btn "afficher plus" selon si le tableaux et a sa taille maximale ou non
            showHide(tabRespFull, '#appendTabResp');
        }, 'json');
    }
    if ($('#tabStud').length){
        $.post('controllers/ajax.php', {
            // la longueur du tableau des étudiant
            countTabStud: document.getElementById('bodyTabStud').rows.length
        }, function (data) {
            // on récupère les infos renvoyé par le php
            var tabStudFull = data.tabStudFull;
            // puis les 2 tableaux on affiche ou non le btn "afficher plus" selon si le tableaux et a sa taille maximale ou non
            showHide(tabStudFull, '#appendTabStud');
        }, 'json');
    }

    // au click  du bouton "afficher plus" sous le tableau des responsables on envoie une requete post
    $('#appendTabResp').click(function () {
        $.post('controllers/ajax.php', {
            appendTabResp: 1,
            // avec le dernier element du tableaux des responsables
            lastElement: document.getElementById('bodyTabResp').rows[document.getElementById('bodyTabResp').rows.length - 1].cells[0].innerHTML
        }, function (data) {
            // on récupère les infos renvoyé par le php
            var newIds = data.newIds;
            var newNames = data.newNames;
            var newMails = data.newMails;
            var maxLength = data.maxLength;
            for (var newRows = 0; newRows < newIds.length; newRows++) {
                // pour chaque nouvelle valeur dans les tableaux on les ajoute à la fin du tableau html.
                $('#bodyTabResp').append('<tr>\n\
                    <td>' + newNames[newRows] + '</td>\n\
                    <td>' + newMails[newRows] + '</td>\n\
                    <td class="text-center downArray"><a href="appR/assets/rapport/rapportTDUE/' + newIds[newRows] + '.pdf" target="_blank"><i class="fas fa-download"></i></a></td>\n\
                </tr>\n');
            }
            if (maxLength) {
                // si la tableaux html a atteint sa taille maximale alors on cache le bouton "afficher plus"
                $("#appendTabResp").hide();
            }
        }, 'json');
    });
    $('#appendTabStud').click(function () {

        $.post('controllers/ajax.php', {
            tabStud: 1,
            // la longueur du tableau des étudiant
            countTab: document.getElementById('bodyTabStud').rows.length
        }, function (data) {
            // on récupère les infos renvoyé par le php
            var maxLength = data.maxLength;
            var newNames = data.newNames;
            var newMails = data.newMails;
            var newRespStuds = data.newRespStuds;
            var newFiles = data.newFiles;
            var withRespStud = data.withRespStud;
            for (var iStud = 0; iStud < newMails.length; iStud++) {
                // pour chaque nouvelle valeur dans les tableaux on les ajoute à la fin du tableau html.
                if(withRespStud){
                    $('#bodyTabStud').append('<tr>\n\
                        <td>' + newNames[iStud] + '</td>\n\
                        <td>' + newMails[iStud] + '</td>\n\
                        <td>' + newRespStuds[iStud] + '</td>\n\
                        <td class="text-center downArray"><a href="appR/assets/rapport/rapportEtudiant/' + newFiles[iStud] + '" target="_blank"><i class="fas fa-download"></i></a></td>\n\
                    </tr>\n');
                }else{
                    $('#bodyTabStud').append('<tr>\n\
                        <td>' + newNames[iStud] + '</td>\n\
                        <td>' + newMails[iStud] + '</td>\n\
                        <td class="text-center downArray"><a href="appR/assets/rapport/rapportEtudiant/' + newFiles[iStud] + '" target="_blank"><i class="fas fa-download"></i></a></td>\n\
                    </tr>\n');
                }
            }
            if (maxLength) {
                // si la tableaux html a atteint sa taille maximale alors on cache le bouton "afficher plus"
                $('#appendTabStud').hide();
            }
        }, 'json');
    });
    function showHide(booleanValue, idElement) {
        if (!booleanValue && $(idElement).is(':visible')) {
            $(idElement).show();
        } else {
            $(idElement).hide();
        }
    }
});