$(function(){
    // function qui cache les elements de la connexions pour le serveur smtp
    function hide_element_login(){
        div_username.hide();
        div_password.hide();
        div_security.hide();
    }
    // les boutons radio
    var is_auth_element = $('#isAuth');
    var not_auth_element = $('#notAuth');
    // les div d'identification
    var div_username = $('#divUsername');
    var div_password = $('#divPassword');
    var div_security = $('#divSecurity');
    // si il n'y à pas besoin de connexion pour le serveur on cache les partit du form
    
    if(!is_auth_element[0].checked){
        hide_element_login();
    }
    // si il clique sur le bouton oui on affiche les partie d'identification
    is_auth_element.click(function(){
        if(is_auth_element[0].checked){
            div_username.show();
            div_password.show();
            div_security.show();
        }
    });
    // si il clique sur le bouton non on cache les partie d'identification
    not_auth_element.click(function(){
       if(not_auth_element[0].checked){
            hide_element_login();
       }
    });
});