<?php
include_once 'config.php';
include_once 'controllers/headerPdfController.php';
?>
<!doctype html>
<html lang="fr">
    <head>
        <meta charset="UTF8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link rel="stylesheet" href="assets/css/style.css">
        <title>publipostage</title>
        <style>
            .headerCategories {
                font-size: 1.5rem;
            }
        </style>
    </head>
    <body class="container-fluid">
        <div class="row">
            <?php
            include_once 'assets/includeHtml/navBar.php';
            ?>
            <div class="col-sm-12 spaceUp">
                <h1>Éditions des entêtes</h1>
                <p>Ces textes seront affichés au début des rapports générés. (À FAIRE AVANT LA GÉNÉRATION)</p>
                <?php
                if (isset($helpMessage) && isset($helpClass)) {
                    ?>
                    <p class="alert <?= $helpClass ?>" role="alert"><?= $helpMessage ?></p>
                    <?php
                }
                ?>
                <!-- student -->
                <form action="headerPDF.php" method="POST" class="spaceUp" enctype="multipart/form-data">
                    <h2 class="headerCategories spaceUp ">Edition pour les rapports des participants.</h2>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="student_first_header">Entête du rapport pour les étudiants :</label>
                        </div>
                        <textarea class="form-control" name="student_first_header" id="student_first_header" required><?= $editableTextsForPdf->student->first_header ?></textarea>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="student_second_header">Texte qui s'affichera sous la note de l'étudiant :</label>
                        </div>
                        <textarea class="form-control" name="student_second_header" id="student_second_header" required><?= $editableTextsForPdf->student->second_header ?></textarea>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="student_mean_prerequis_title">Titre du paragraphe qui liste les moyennes des prérequis disciplinaires du participant :</label>
                        </div>
                        <textarea class="form-control" name="student_mean_prerequis_title" id="student_mean_prerequis_title" required><?= $editableTextsForPdf->student->mean_prerequis_title ?></textarea>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="student_mean_registred_ue_title">Titre du paragraphe qui liste les moyennes des UE où le participant est inscrit :</label>
                        </div>
                        <textarea class="form-control" name="student_mean_registred_ue_title" id="student_mean_registred_ue_title" required><?= $editableTextsForPdf->student->mean_registred_ue_title ?></textarea>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="student_detail_ue_title">Titre du paragraphe des détails des prérequis des UE où le participant est inscrit :</label>
                        </div>
                        <textarea class="form-control" name="student_detail_ue_title" id="student_detail_ue_title" required><?= $editableTextsForPdf->student->detail_ue_title ?></textarea>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="student_mean_unregistred_ue_title">Titre du paragraphe qui liste les moyennes des UE où le participant n'est pas inscrit :</label>
                        </div>
                        <textarea class="form-control" name="student_mean_unregistred_ue_title" id="student_mean_unregistred_ue_title" required><?= $editableTextsForPdf->student->mean_unregistred_ue_title ?></textarea>
                    </div>
                    <!-- UE -->
                    <h2 class="headerCategories spaceUp ">Édition pour les rapports des responsables d'UE.</h2>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="ue_document_title">Titre du rapport pour les responsables d'UE :</label>
                        </div>
                        <textarea class="form-control" name="ue_document_title" id="ue_document_title" required><?= $editableTextsForPdf->ue->document_title ?></textarea>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="ue_header">Entête du rapport pour les responsables d'UE :</label>
                        </div>
                        <textarea class="form-control" name="ue_header" id="ue_header" required><?= $editableTextsForPdf->ue->header ?></textarea>
                    </div>
                    <!-- TD -->
                    <h2 class="headerCategories spaceUp ">Édition pour les rapprots des responsables de TD/TP.</h2>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="td_document_title">Titre du rapport pour les responsables de TD/TP :</label>
                        </div>
                        <textarea class="form-control" name="td_document_title" id="td_document_title" required><?= $editableTextsForPdf->td->document_title ?></textarea>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="td_header">Entête du rapport pour les responsables de TD/TP :</label>
                        </div>
                        <textarea class="form-control" name="td_header" id="td_header" required><?= $editableTextsForPdf->td->header ?></textarea>
                    </div>
                    <!-- GLOBAL -->
                    <h2 class="headerCategories spaceUp ">Édition pour le rapport global.</h2>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="global_document_title">Titre du rapport global :</label>
                        </div>
                        <textarea class="form-control" name="global_document_title" id="global_document_title" required><?= $editableTextsForPdf->global->document_title ?></textarea>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="global_header">Entête du rapport global :</label>
                        </div>
                        <textarea class="form-control" name="global_header" id="global_header" required><?= $editableTextsForPdf->global->header ?></textarea>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="global_list_prerequis_title">Titre du paragraphe qui liste les moyennes des prérequis des participants :</label>
                        </div>
                        <textarea class="form-control" name="global_list_prerequis_title" id="global_list_prerequis_title" required><?= $editableTextsForPdf->global->list_prerequis_title ?></textarea>
                    </div>
                    <!-- Logo  -->
                    <h2 class="headerCategories spaceUp ">Édition du logo en haut des rapports.</h2>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="logo_file">Logo affiché en haut droite des courriers :</label>
                        </div>
                        <input type="file" class="form-control" name="logo_file" id="logo_file" accept="image/png, image/jpeg"/>
                    </div>
                    <div class="col-sm-12">
                        <input type="submit" name="updateHeader" class="btn btn-primary" value="Enregistrer"/>
                    </div>
                </form>
            </div>
        </div>
        <script src="assets/lib/jquery/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="assets/lib/popper/popper.min.js" type="text/javascript"></script>
        <script src="assets/lib/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>