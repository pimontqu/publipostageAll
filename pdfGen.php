<?php
include_once 'config.php';
include_once 'utils/utils.php';
include_once 'utils/readCsvScores.php';
include_once 'utils/readCsvResp.php';
include_once 'controllers/pdfGenController.php';
?>
<!doctype html>
<html lang="fr">
    <head>
        <meta charset="UTF8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
        <title>publipostage</title>
    </head>
    <body class="container-fluid">
        <div class="row">
            <?php
            include_once 'assets/includeHtml/navBar.php';
            ?>
            <div class="col-sm-12 spaceUp">
                <div class="col-sm-4 offset-sm-8 d-flex align-items-center">
                    <img src="assets/images/logo-unisciel.png" alt="">
                </div>
            </div>
            <div class="col-sm-12 spaceUp">
                <form enctype="multipart/form-data" action="pdfGen.php" method="POST" class="row">
                    <div class="col-sm-8">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="scoreFile">Implémenter le fichier de notes (.odt) *</label>
                            </div>
                            <input class="form-control-file" type="file" name="scoreFile" id="scoreFile" required>
                        </div>
                        <div class="form-group" style="display: none">
                            <div class="col-sm-12">
                                <label for="userFile">Fichier des utilisateurs</label>
                            </div>
                            <input class="form-control-file" type="file" name="userFile" id="userFile">
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="respFile">Implémenter le fichier des groupes (.odt)</label>
                            </div>
                            <input class="form-control-file" type="file" name="respFile" id="respFile">
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="grpQFile">Liste des thèmes disciplinaires testés *</label>
                            </div>
                            <input class="form-control-file" type="file" name="grpQFile" id="grpQFile" required>
                        </div>
                        <?php
                        if(count($dirReportGlobal) > 3 && count($dirReportStud) > 3) {
                            ?>
                            <div class="form-check">
                                <div class="col-sm-12">
                                    <input class="form-check-input" type="checkbox" name="lastGen" id="lastGen">
                                    <label for="lastGen" class="form-check-label">Récupérer la dernière génération?</label>
                                    <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="right" title="Utiles si vous n'avez rien changer à vos fichiers mais que voulez revoir la dernière génération sans pour autant la refaire"></i>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="col-sm-4 d-flex align-items-center">
                        <div class="form-group">
                            <input class="btn btn-primary"type="submit" name="generate" value="Générer les rapports">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <?php
        if(isset($error_link_ref) && $error_link_ref){
        ?>
        <div class="row spaceUp">
            <div class="col-sm-10 offset-md-1 alert alert-danger" role="alert">
                <p>Il y des erreurs dans les fichiers csv les rapport ne sont pas générés. Voici les erreurs détectées :</p>
        <?php
            if(isset($error_uv) && $error_uv !== false){
                ?>
                    <ul>
                        <?php
                        $i_uv_error = 0;
                        foreach($error_uv['uvs'] as $uv_not_found){
                            ?>
                            <li>L'uv <strong><?= $uv_not_found ?></strong> n'a pas été trouvé dans le fichier de listing des uvs pour la question <strong>"<?= $error_uv['questions'][$i_uv_error] ?>"</strong> à la colonne <strong><?= $error_uv['id_col'][$i_uv_error] + 1 ?></strong> du carnet de notes</li>
                            <?php
                            $i_uv_error++;
                        }
                        ?>
                    </ul>
                <?php
            }
            if(isset($error_grp) && $error_grp !== false){
                ?>
                    <ul>
                        <?php
                        $i_grp_error = 0;
                        foreach($error_grp['grp'] as $grp_not_found){
                            ?>
                            <li>Le thème de questions <strong>"<?= $grp_not_found ?>"</strong> n'a pas été trouvé dans le fichier de listing des thèmes de questions pour la question <strong>"<?= $error_grp['questions'][$i_grp_error] ?>"</strong> à la colonne <strong><?= $error_grp['id_col'][$i_grp_error] + 1 ?></strong> du carnet de notes </li>
                            <?php
                            $i_grp_error++;
                        }
                        ?>
                    </ul>
                <?php
            }
            if(isset($col_error_score) && $col_error_score !== false){
                ?>
                <ul>
                <?php
                foreach ($col_error_score as $error_score) {
                    ?>
                    <li>La colonne <strong>"<?= $error_score ?>"</strong> n'a pas été trouvé dans le carnets de notes</li>
                    <?php
                }
                ?>
                </ul>
                <?php
            }
            if(isset($id_error_uv) && $id_error_uv !== false){
                ?>
                <ul>
                <?php
                $i_error_resp = 0;
                foreach ($id_error_uv['id_resp'] as $error_resp) {
                    ?>
                    <li>L'id <strong>"<?= $error_resp ?>"</strong> à la ligne <strong>"<?= $id_error_uv['nb_line'][$i_error_resp] ?>"</strong> n'est pas un id valide pour le fichier de groupe d'uv et td</li>
                    <?php
                    $i_error_resp++;
                }
                ?>
                </ul>
                <?php
            }
            if(isset($col_error_grp) && $col_error_grp !== false){
                ?>
                <ul>
                <?php
                foreach ($col_error_grp as $error_grp) {
                    ?>
                    <li>La colonne <strong>"<?= $error_grp ?>"</strong> n'a pas été trouvé dans le listing des thèmes de questions </li>
                    <?php
                }
                ?>
                </ul>
                <?php
            }
        }
        ?>
            </div>
        </div>
        <?php
        // si les rapport on été généré
        if (count($_SESSION) > 0 && ($generate || array_key_exists('sendMailResp', $_GET) || array_key_exists('sendMailStud', $_GET) || array_key_exists('sendMailRespStud', $_GET) || array_key_exists('sendAll', $_GET))) {
            ?>
            <div class="row spaceUp">
                <div class="col-sm-9">
                    <?php
                    // si le bouton envoier tous les mails est appuyé
                    if (array_key_exists('sendAll', $_GET)) {
                        if (isset($responseAllResp) && count($responseAllResp['mailInvalid']) > 0) {
                            ?>
                            <p class="alert alert-warning" role="alert">Les mails suivant n'ont pas pu être expédié aux résponsables d'UE/UV et/ou de TD/TP, car leurs adresses de courriels n'est pas valide :</p>
                            <ul class="alert alert-warning" role="alert">
                                <?php
                                foreach ($responseAllResp['mailInvalid'] as $invalidMailResp) {
                                    ?>
                                    <li><?= $invalidMailResp ?></li>
                                    <?php
                                }
                                ?>
                            </ul>
                            <?php
                        } else {
                            ?>
                            <p role="alert" class="<?= isset($classPResp) ? $classPResp : '' ?>"><?= isset($feedBackResp) ? $feedBackResp : '' ?></p>
                            <?php
                        }
                        if (isset($responseAllRespStud) && count($responseAllRespStud['mailInvalid']) > 0) {
                            ?>
                            <p class="alert alert-warning" role="alert">Les mails suivant n'ont pas pu être expédié aux Tuteurs, car leurs adresses de courriels n'est pas valide :</p>
                            <ul class="alert alert-warning" role="alert">
                                <?php
                                foreach ($responseAllRespStud['mailInvalid'] as $invalidMailRespStud) {
                                    ?>
                                    <li><?= $invalidMailRespStud ?></li>
                                    <?php
                                }
                                ?>
                            </ul>
                            <?php
                        } else {
                            ?>
                            <p role="alert" class="<?= isset($classPRespStud) ? $classPRespStud : '' ?>"><?= isset($feedBackRespStud) ? $feedBackRespStud : '' ?></p>
                            <?php
                        }
                        if (isset($responseAllStud) && count($responseAllStud['mailInvalid']) > 0) {
                            ?>
                            <p class="alert alert-warning" role="alert">Les mails suivant n'ont pas pu être expédié aux étudiants, car leurs adresses de courriels n'est pas valide :</p>
                            <ul class="alert alert-warning" role="alert">
                                <?php
                                foreach ($responseAllStud['mailInvalid'] as $invalidMailStud) {
                                    ?>
                                    <li><?= $invalidMailStud ?></li>
                                    <?php
                                }
                                ?>
                            </ul>
                            <?php
                        } else {
                            ?>
                            <p role="alert" class="<?= $classPStud ?>"><?= $feedBackStud ?></p>
                            <?php
                        }
                        ?>
                        <?php
                    }
                    // si le bouton envoyer les mails aux responsable est appuyé
                    if (array_key_exists('sendMailResp', $_GET)) {
                        if (count($responseResp['mailInvalid']) > 0) {
                            ?>
                            <p class="alert alert-warning" role="alert">Les mails suivant n'ont pas pu être expédié car leurs adresses de courriels n'est pas valide :</p>
                            <ul class="alert alert-warning" role="alert">
                                <?php
                                foreach ($responseResp['mailInvalid'] as $mailInvalid) {
                                    ?>
                                    <li><?= $mailInvalid ?></li>
                                    <?php
                                }
                                ?>
                            </ul>
                            <?php
                        } else {
                            ?>
                            <p id="feedBackResp" role="alert" class="<?= $responseResp['class'] ?>"><?= $responseResp['feedBack'] ?></p>
                            <?php
                        }
                    }
                    // si le bouton envoie mails aux étudiants est envoyer
                    if (array_key_exists('sendMailStud', $_GET)) {
                        if (count($responseStud['mailInvalid']) > 0) {
                            ?>
                            <p class="alert alert-warning" role="alert">Les mails suivant n'ont pas pu être expédié car leurs adresses de courriels n'est pas valide :</p>
                            <ul class="alert alert-warning" role="alert">
                                <?php
                                foreach ($responseStud['mailInvalid'] as $mailInvalid) {
                                    ?>
                                    <li><?= $mailInvalid ?></li>
                                    <?php
                                }
                                ?>
                            </ul>
                            <?php
                        } else {
                            ?>
                            <p id="feedBackStud" role="alert" class="<?= $responseStud['class'] ?>"><?= $responseStud['feedBack'] ?></p>
                            <?php
                        }
                    }
                    // si le bouton envoie mails référent et appuyé
                    if (array_key_exists('sendMailRespStud', $_GET)) {
                        if (count($responseRespStud['mailInvalid']) > 0) {
                            ?>
                            <p class="alert alert-warning" role="alert">Les mails suivant n'ont pas pu être expédié car leurs adresses de courriels n'est pas valide :</p>
                            <ul class="alert alert-warning" role="alert">
                                <?php
                                foreach ($responseRespStud['mailInvalid'] as $mailInvalid) {
                                    ?>
                                    <li><?= $mailInvalid ?></li>
                                    <?php
                                }
                                ?>
                            </ul>
                            <?php
                        } else {
                            ?>
                            <p role="alert" class="<?= $responseRespStud['class'] ?>"><?= $responseRespStud['feedBack'] ?></p>
                            <?php
                        }
                    }
                    ?>
                    <h2>Téléchargez tous les rapports  : <a href="<?= $linkZipFile  ?>"><i class="fas fa-download"></i></a></h2>
                    <h2 class="spaceUp">Rapport Global : <a href="appR/assets/rapport/rapportGlobale/globale.pdf" target="_blank"><i class="fas fa-download"></i></a></h2>
                </div>
                <div class="col-sm-3">
                    <a href="pdfGen.php?sendAll=1" class="btn btn-primary">Envoie de tous les mails</a>
                </div>
            </div>
            <?php
        }
        // si les rapport on été généré
        if (array_key_exists('listIdTDnUE', $_SESSION) && $_SESSION['withResp'] && ($generate || array_key_exists('sendMailResp', $_GET) || array_key_exists('sendMailStud', $_GET) || array_key_exists('sendMailRespStud', $_GET) || array_key_exists('sendAll', $_GET))) {
            ?>
            <div class="row spaceUp">
                <div class="col-sm-9">
                    <h2>Rapport Responsable UE, TD et TP</h2>
                    <table id="tabResp" class="table">
                        <thead>
                            <tr>
                                <th scope="col">Nom TD/UE</th>
                                <th scope="col">Adresse de courriel</th>
                                <th scope="col" class="text-center">Fichier</th>
                            </tr>
                        </thead>
                        <tbody id="bodyTabResp">
                            <?php
                            $tableRespCount = 0;
                            for($iResp = 0; $tableRespCount < 200 && $iResp < count($_SESSION['listIdTDnUE']); $iResp++) {
                                foreach ($_SESSION['listRespTDnUE'] as $listMail) {
                                    $currentMail = $listMail[$_SESSION['listIdTDnUE'][$iResp]];
                                    if (!empty($currentMail)) {
                                        ?>
                                        <tr>
                                            <td><?= $_SESSION['listNameTDnUE'][$_SESSION['listIdTDnUE'][$iResp]] ?></td>
                                            <td scope="row"><?= $currentMail ?></td>
                                            <td class="text-center downArray"><a href="<?= 'appR/assets/rapport/rapportTDUE/' . str_replace(array(' ', '-'), '.', $_SESSION['listIdTDnUE'][$iResp]) . '.pdf' ?>" target="_blank"><i class="fas fa-download"></i></a></td>
                                        </tr>
                                        <?php
                                        $tableRespCount++;
                                    }
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                    <button class="btn btn-primary" id="appendTabResp">afficher plus</button>
                </div>
                <div class="col-sm-3">
                    <a href="pdfGen.php?sendMailResp=1"><button id="sendMailResp" class="btn btn-primary">Envoyer les mails aux responsables</button></a>
                </div>
            </div>
            <?php
        }
        // si les rapports sont généré
        if (array_key_exists('mailStud', $_SESSION) && ($generate || array_key_exists('sendMailResp', $_GET) || array_key_exists('sendMailStud', $_GET) || array_key_exists('sendMailRespStud', $_GET) || array_key_exists('sendAll', $_GET))) {
            ?>
            <div class="row spaceUp">
                <div class="col-sm-9">
                    <h2>Rapport Étudiants (<?= count($_SESSION['mailStud']) ?> générés)</h2>
                    <table id="tabStud" class="table">
                        <thead>
                            <tr>
                                <th scope="col">Nom</th>
                                <th scope="col">Adresse de courriel</th>
                                <?php
                                if ($_SESSION['withRespStud']) {
                                    ?>
                                    <th scope="col">Tuteur</th>
                                    <?php
                                }
                                ?>
                                <th scope="col" class="text-center">Fichier</th>
                            </tr>
                        </thead>
                        <tbody id="bodyTabStud">
                            <?php
                            // on affiche la liste des étudiants dans un tableaux
                            for ($iStudent = 0; $iStudent < 200 && $iStudent < count($_SESSION['mailStud']); $iStudent++) {
                                ?>
                                <tr>
                                    <td scope="row"><?= $_SESSION['firstNameStud'][$iStudent] . ' ' . $_SESSION['lastNameStud'][$iStudent] ?></td>
                                    <td><?= $_SESSION['mailStud'][$iStudent] ?></td>
                                    <?php
                                    if ($_SESSION['withRespStud']) {
                                        ?>
                                        <td><?= $_SESSION['mailRespStud'][$iStudent] ?></td>
                                        <?php
                                    }
                                    ?>
                                    <td class="text-center downArray"><a href="<?= 'appR/assets/rapport/rapportEtudiant/' . $_SESSION['firstNameStud'][$iStudent] . '_' . $_SESSION['lastNameStud'][$iStudent] . '.pdf' ?>" target="_blank"><i class="fas fa-download"></i></a></td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                    <button class="btn btn-primary" id="appendTabStud">afficher plus</button>
                </div>
                <div class="col-sm-3">
                    <a href="pdfGen.php?sendMailStud=1"><button id="sendMailStud" class="btn btn-primary">envoyer les mails aux étudiants</button></a>
                    <?php
                    if ($_SESSION['withRespStud']) {
                        ?>
                        <a href="pdfGen.php?sendMailRespStud=1"><button id="sendMailRespStud" class="btn btn-primary spaceUp">Envoyer les mails aux tuteurs</button></a>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <?php
        }
        ?>
        <script src="assets/lib/jquery/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="assets/lib/popper/popper.min.js" type="text/javascript"></script>
        <script src="assets/lib/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/js/appendTab.js" type="text/javascript"></script>
        <script src="assets/js/log.js" type="text/javascript"></script>
    </body>
</html>