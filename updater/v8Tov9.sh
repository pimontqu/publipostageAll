sudo apt update
sudo apt install -y libharfbuzz-dev  libfribidi-dev

sudo git config --global --add safe.directory /var/www/html/publi
cd /var/www/html/publi/
sudo cp /var/www/html/publi/texteRapport-template/texteRapport.json /var/www/html/publi/texteRapport/texteRapport.json
sudo chown -R www-data:www-data /var/www/html/publi/texteRapport/
sudo chown -R www-data:www-data /var/www/html/publi/appR/assets/images/


sudo su - -c "R -q -e \"install.packages('curl')\""
sudo su - -c "R -q -e \"install.packages('rversions')\""
sudo su - -c "R -q -e \"install.packages('rprojroot')\""
sudo su - -c "R -q -e \"install.packages('desc')\""
sudo su - -c "R -q -e \"install.packages('textshaping')\""
sudo su - -c "R -q -e \"install.packages('mime')\""
sudo su - -c "R -q -e \"install.packages('bslib')\""
sudo su - -c "R -q -e \"install.packages('shiny')\""
sudo su - -c "R -q -e \"install.packages('miniUI')\""
sudo su - -c "R -q -e \"install.packages('prettyunits')\""
sudo su - -c "R -q -e \"install.packages('pkgbuild')\""
sudo su - -c "R -q -e \"install.packages('htmlwidgets')\""
sudo su - -c "R -q -e \"install.packages('profvis')\""
sudo su - -c "R -q -e \"install.packages('ragg')\""
sudo su - -c "R -q -e \"install.packages('rcmdcheck')\""
sudo su - -c "R -q -e \"install.packages('pkgdown')\""
sudo su - -c "R -q -e \"install.packages('jsonlite')\""
sudo su - -c "R -q -e \"install.packages('rmarkdown')\""
sudo su - -c "R -q -e \"install.packages('rjson')\""
sudo su - -c "R -q -e \"install.packages('pkgload')\""
sudo su - -c "R -q -e \"install.packages('roxygen2')\""
sudo su - -c "R -q -e \"install.packages('testthat')\""
sudo su - -c "R -q -e \"install.packages('devtools')\""
sudo su - -c "R -q -e \"remotes::install_github(c('ropensci/tabulizerjars', 'ropensci/tabulizer'))\""
sudo su - -c "R -q -e \"devtools::install_github('ricardo-bion/ggradar', dependencies = TRUE)\""