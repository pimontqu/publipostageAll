sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
sudo add-apt-repository 'deb https://cloud.r-project.org/bin/linux/ubuntu bionic-cran40/'

sudo apt update
sudo apt upgrade -y
sudo apt install default-jre default-jdk -y

sudo su - -c "R -q -e \"install.packages('utf8')\""
sudo su - -c "R -q -e \"install.packages('yaml')\""
sudo su - -c "R -q -e \"install.packages('labeling')\""
sudo su - -c "R -q -e \"install.packages('tinytex')\""
sudo su - -c "R -q -e \"install.packages('highr')\""
sudo su - -c "R -q -e \"install.packages('stringi')\""
sudo su - -c "R -q -e \"install.packages('magrittr')\""
sudo su - -c "R -q -e \"install.packages('stringr')\""
sudo su - -c "R -q -e \"install.packages('digest')\""
sudo su - -c "R -q -e \"install.packages('htmltools')\""
sudo su - -c "R -q -e \"install.packages('knitr')\""
sudo su - -c "R -q -e \"install.packages('rmarkdown')\""
sudo su - -c "R -q -e \"install.packages('Rcpp')\""
sudo su - -c "R -q -e \"install.packages('pander')\""
sudo su - -c "R -q -e \"install.packages('withr')\""
sudo su - -c "R -q -e \"install.packages('crayon')\""
sudo su - -c "R -q -e \"install.packages('gtable')\""
sudo su - -c "R -q -e \"install.packages('R6')\""
sudo su - -c "R -q -e \"install.packages('colorspace')\""
sudo su - -c "R -q -e \"install.packages('munsell')\""
sudo su - -c "R -q -e \"install.packages('scales')\""
sudo su - -c "R -q -e \"install.packages('glue')\""
sudo su - -c "R -q -e \"install.packages('pkgconfig')\""
sudo su - -c "R -q -e \"install.packages('tibble')\""
sudo su - -c "R -q -e \"install.packages('ggplot2')\""
sudo su - -c "R -q -e \"install.packages('xtable')\""
sudo su - -c "R -q -e \"install.packages('xlsx')\""
sudo su - -c "R -q -e \"install.packages('png')\""
sudo su - -c "R -q -e \"install.packages('tabulizer')\""
