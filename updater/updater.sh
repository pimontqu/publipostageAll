#! /bin/bash

cd "$(dirname "$0")"
cd ..

pauseexit () {
	if [ "x$1" == "x0" ] ; then
		echo "installation terminée"
	else
		echo "une erreur a été rencontrée lors de la mise à jour. Contactez stephane.poinsart@utc.fr"
	fi
	echo "Appuyez sur une touche pour continuer"
	read
	exit $1
}


if [ ! -f ./updater/ENVVERSION ] ; then
	sudo cp ./updater/CODEVERSION ./updater/ENVVERSION
fi


if zenity --question --text "Voulez vous mettre à jour l'outil de publipostage ?" 2>/dev/null ; then
	sudo git stash
	sudo git checkout prod
	sudo bash -c "git pull || { echo 'Erreur de récupération de la nouvelle version du programme. Pb de connexion internet ? Pb de fichiers modifiés en conflits ?' ; exit 1 ; }" || pauseexit 1
	sudo chmod +x ./updater/migrate.sh
	sudo ./updater/migrate.sh
	pauseexit 0
fi


