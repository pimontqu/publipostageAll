sudo apt-get update
#! /bin/bash

cd "$(dirname "$0")"
cd ../

if [ ! -d "texteRapport" ] ; then
    sudo cp -r "texteRapport-template" "texteRapport"
else
    pwd
    if [ ! -f "texteRapport/etudiant_0_50.txt" ] ; then
        sudo cp "texteRapport-template/etudiant_0_50.txt" "texteRapport/"
    fi
    
    if [ ! -f "texteRapport/etudiant_50_75.txt" ] ; then
        sudo cp "texteRapport-template/etudiant_50_75.txt" "texteRapport/"
    fi
    
    if [ ! -f "texteRapport/etudiant_75_100.txt" ] ; then
        sudo cp "texteRapport-template/etudiant_75_100.txt" "texteRapport/"
    fi
    
    if [ ! -f "texteRapport/global.txt" ] ; then
        sudo cp "texteRapport-template/global.txt" "texteRapport/"
    fi

    if [ ! -f "texteRapport/TD.txt" ] ; then
        sudo cp "texteRapport-template/TD.txt" "texteRapport/"
    fi
    
    if [ ! -f "texteRapport/UE.txt" ] ; then
        sudo cp "texteRapport-template/UE.txt" "texteRapport/"
    fi
fi

if [ ! -f "config.php" ] ; then
    sudo cp "config.php.template" "config.php"
fi

cd
cd "Desktop"

tkInstalled=$(dpkg -s python3-tk | grep Status)

if [ "$tkInstalled" != "Status: install ok installed" ] ; then
    sudo apt install python3-tk -y
fi

cp "/var/www/html/publi/updater/créer partage" "créer dossier partagé"

if [ ! -f "procedure-dossier-partage.pdf" ] ; then
    ln -s "/var/www/html/publi/procedure-dossier-partage.pdf"
fi