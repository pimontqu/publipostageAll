#! /bin/bash

# applique des changements au système, si nécessaire pour faire fonctionner une nouvelle version
# (apt, config apache/php, ...)

envver=$( cat updater/ENVVERSION )
codever=$( cat updater/CODEVERSION )

echo "version du programme : " $codever
echo "version de l'environement : " $envver

while (( envver < codever )) ; do
	echo "Mise à jour depuis version $envver"
	case $envver in
		1)
		cd "/var/www/html/publi/"
		sudo chmod +x ./updater/v1ToV2.sh
		sudo ./updater/v1ToV2.sh
			;;
		2)
		cd "/var/www/html/publi/"
		sudo chmod +x ./updater/v2ToV3.sh
		sudo ./updater/v2ToV3.sh
			;;
		3)
		cd "/var/www/html/publi/"
		sudo chmod +x ./updater/v3ToV4.sh
		sudo ./updater/v3ToV4.sh
			;;
		4)
		cd "/var/www/html/publi/"
		sudo chmod +x ./updater/v4ToV5.sh
		sudo ./updater/v4ToV5.sh
			;;
		5)
		cd "/var/www/html/publi/"
		sudo chmod +x ./updater/v5ToV6.sh
		sudo ./updater/v5ToV6.sh
			;;
		6)
		cd "/var/www/html/publi/"
		sudo chmod +x ./updater/v6Tov7.sh
		sudo ./updater/v6Tov7.sh
			;;
		7)
		cd "/var/www/html/publi/"
		sudo chmod +x ./updater/v7Tov8.sh
		sudo ./updater/v7Tov8.sh
			;;
		8)
		cd "/var/www/html/publi/"
		sudo chmod +x ./updater/v8Tov9.sh
		sudo ./updater/v8Tov9.sh
			;;
	esac
	let "envver += 1"
	echo $envver > updater/ENVVERSION
done
##à toujours laissée à la fin.
cd "/var/www/html/"
chown www-data:www-data -R publi